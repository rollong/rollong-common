package com.rollong.common.ioc.spring

import org.springframework.beans.BeansException
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles
import org.springframework.stereotype.Component

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 1:03
 * @project rollong-common
 * @filename EnvironmentResolver.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.util.ioc
 * @description
 */
@Component
class EnvironmentResolver : EnvironmentAware {

    @Throws(BeansException::class)
    override fun setEnvironment(environment: Environment) {
        Companion.environment = environment
    }

    companion object {
        private var environment: Environment? = null

        fun getEnvironment(): Environment? {
            return environment
        }

        fun acceptsProfile(profile: String): Boolean {
            return this.getEnvironment()!!.acceptsProfiles(Profiles.of(profile))
        }

        fun acceptsAnyProfiles(vararg profiles: String): Boolean {
            return this.getEnvironment()!!.acceptsProfiles(
                this.profiles(profiles.toList(), "|")
            )
        }

        fun acceptsAllProfiles(vararg profiles: String): Boolean {
            return this.getEnvironment()!!.acceptsProfiles(
                this.profiles(profiles.toList(), "&")
            )
        }

        fun acceptsNoneProfiles(vararg profiles: String): Boolean {
            return this.getEnvironment()!!.acceptsProfiles(
                this.profiles(profiles.toList().map { "(! $it)" }, "&")
            )
        }

        private fun profiles(list: List<String>, glue: String): Profiles {
            val sb = StringBuffer()
            list.forEach {
                if (sb.isEmpty()) {
                    sb.append(it)
                } else {
                    sb.append(" $glue $it")
                }
            }
            return Profiles.of(sb.toString())
        }
    }
}