package com.rollong.common.ioc.spring.event

import com.rollong.common.ioc.spring.ApplicationContextResolver
import org.springframework.context.ApplicationEvent

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 12:49
 * @project rollong-common
 * @filename Event.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.util.ioc.event
 * @description
 */
abstract class Event(source: Any) : ApplicationEvent(source) {
    fun publish() {
        ApplicationContextResolver.getApplicationContext()!!.publishEvent(this)
    }
}