package com.rollong.common.ioc.spring

import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 1:01
 * @project rollong-common
 * @filename ApplicationContextResolver.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.util.ioc
 * @description
 */
fun <T> Class<T>.getApplicationBean() = ApplicationContextResolver.getApplicationContext()!!.getBean(this)

fun <T : Any> KClass<T>.getApplicationBean() = ApplicationContextResolver.getApplicationContext()!!.getBean(this.java)

@Component
class ApplicationContextResolver : ApplicationContextAware {

    @Throws(BeansException::class)
    override fun setApplicationContext(applicationContext: ApplicationContext) {
        context = applicationContext
    }

    companion object {
        private var context: ApplicationContext? = null
        fun getApplicationContext(): ApplicationContext? {
            return context
        }
    }
}