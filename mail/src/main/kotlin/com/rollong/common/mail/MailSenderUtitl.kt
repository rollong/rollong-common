package com.rollong.common.mail

import java.util.*
import javax.mail.Session
import javax.mail.internet.MimeMessage

object MailSenderUtitl {

    fun session(
        smtpHost: String,
        debug: Boolean = true
    ): Session {
        //1、连接邮件服务器的参数配置
        val props = Properties()
        //设置用户的认证方式
        props.setProperty("mail.smtp.auth", "true")
        //设置传输协议
        props.setProperty("mail.transport.protocol", "smtp")
        //设置发件人的SMTP服务器地址
        props.setProperty("mail.smtp.host", smtpHost)
        //2、创建定义整个应用程序所需的环境信息的 Session 对象
        val session = Session.getInstance(props)
        //设置调试信息在控制台打印出来
        session.debug = debug
        return session
    }

    fun send(
        session: Session,
        sender: String,
        password: String,
        message: MimeMessage
    ) {
        val transport = session.transport
        try {
            transport.connect(sender, password)
            transport.sendMessage(message, message.allRecipients)
        } catch (e: Throwable) {
            throw e
        } finally {
            transport.close()
        }
    }
}