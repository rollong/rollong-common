package com.rollong.common.mail.mime

import org.springframework.core.io.InputStreamSource
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.activation.DataSource
import javax.mail.internet.ContentType

class ByteArrayDataSource(
    private val name: String,
    private val contentType: ContentType,
    private var byteArray: ByteArray
) : DataSource, InputStreamSource {

    override fun getContentType() = this.contentType.toString()

    override fun getInputStream() = ByteArrayInputStream(this.byteArray)

    override fun getName() = this.name

    override fun getOutputStream() = ByteArrayOutputStream()
}