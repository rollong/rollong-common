package com.rollong.common.mail.mime

import javax.activation.DataHandler
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMultipart
import javax.mail.internet.MimeUtility

object MimeUtil {

    fun bodyPart(
        dataHandler: DataHandler? = null,
        content: String? = null,
        contentID: String? = null
    ): MimeBodyPart {
        val mimeBodyPart = MimeBodyPart()
        dataHandler?.let {
            mimeBodyPart.dataHandler = it
        }
        content?.let {
            mimeBodyPart.setContent(content, "text/html;charset=UTF-8")
        }
        contentID?.let {
            mimeBodyPart.contentID = it
        }
        return mimeBodyPart
    }

    fun attachment(
        dataHandler: DataHandler
    ): MimeBodyPart {
        val mimeBodyPart = MimeBodyPart()
        mimeBodyPart.dataHandler = dataHandler
        mimeBodyPart.fileName = MimeUtility.encodeText(dataHandler.name)
        return mimeBodyPart
    }

    fun multipart(
        bodyParts: List<MimeBodyPart>,
        subtype: String
    ): MimeMultipart {
        val multipart = MimeMultipart()
        bodyParts.forEach {
            multipart.addBodyPart(it)
        }
        multipart.setSubType(subtype)
        return multipart
    }

    fun mail(
        content: String,
        attachments: List<DataHandler>
    ): MimeMultipart {
        val parts = mutableListOf(bodyPart(content = content))
        parts.addAll(attachments.map { attachment(it) })
        return multipart(bodyParts = parts, subtype = "mixed")
    }
}