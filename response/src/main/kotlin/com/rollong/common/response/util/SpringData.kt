package com.rollong.common.response.util

import com.rollong.common.response.GenericPageableResponse
import org.springframework.data.domain.Page

fun <T> Page<T>.toPageableResponse(): GenericPageableResponse<T> {
    return GenericPageableResponse(
        items = this.toList(),
        totalElements = this.totalElements,
        totalPages = this.totalPages,
        currentPage = this.number,
        pageSize = this.size
    )
}