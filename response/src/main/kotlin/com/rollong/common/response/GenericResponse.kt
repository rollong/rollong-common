package com.rollong.common.response


data class GenericResponse<T> constructor(
    override var code: Int = 0,
    override var errCode: String? = null,
    override var message: String = "",
    var items: T? = null
) : Response {

    constructor(code: Int? = 0, errCode: String? = null, message: String, items: T? = null) : this() {
        this.code = code ?: 0
        this.errCode = errCode
        this.message = message
        this.items = items
    }

    constructor(code: Int? = 0, message: String, items: T? = null) : this() {
        this.code = code ?: 0
        this.message = message
        this.items = items
    }

    constructor(code: Int? = 0, message: String) : this() {
        this.code = code ?: 0
        this.message = message
    }

    constructor(items: T) : this() {
        this.items = items
    }

}