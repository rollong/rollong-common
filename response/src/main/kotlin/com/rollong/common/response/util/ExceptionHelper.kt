package com.rollong.common.response.util

import com.rollong.common.exception.APICallException
import com.rollong.common.exception.BaseException
import com.rollong.common.response.GenericPageableResponse
import com.rollong.common.response.GenericResponse
import java.util.*
import java.util.function.Supplier

fun <T : Any> GenericResponse<T>.getOrThrow(exceptionSupplier: Supplier<Throwable>? = null): Optional<T> {
    return if (this.code == 0) {
        this.items?.let { Optional.of(it) } ?: Optional.empty()
    } else {
        val e = (exceptionSupplier?.get() ?: APICallException())
        if (e is BaseException) {
            e.code = this.code
            this.errCode?.let { e.errCode = it }
            if (this.message.isNotBlank()) {
                e.message = this.message
            }
        }
        if (e is APICallException) {
            e.serverErrCode = this.errCode
            e.serverErrMessage = this.message
        }
        throw e
    }
}

fun <T : Any> GenericPageableResponse<T>.getOrThrow(exceptionSupplier: Supplier<Throwable>? = null): List<T> {
    return if (this.code == 0) {
        this.items
    } else {
        val e = (exceptionSupplier?.get() ?: APICallException())
        if (e is BaseException) {
            e.code = this.code
            this.errCode?.let { e.errCode = it }
            if (this.message.isNotBlank()) {
                e.message = this.message
            }
        }
        if (e is APICallException) {
            e.serverErrCode = this.errCode
            e.serverErrMessage = this.message
        }
        throw e
    }
}

inline fun <reified T : Any> Throwable.toResponse(): GenericResponse<T> {
    return if (this is BaseException) {
        GenericResponse(
            code = this.code,
            errCode = this.errCode,
            message = this.message ?: this.javaClass.name
        )
    } else {
        GenericResponse(
            code = 500,
            message = this.message ?: this.javaClass.name
        )
    }
}

inline fun <reified T : Any> Throwable.toPageableResponse(): GenericPageableResponse<T> {
    return if (this is BaseException) {
        GenericPageableResponse(
            code = this.code,
            errCode = this.errCode,
            message = this.message ?: this.javaClass.name
        )
    } else {
        GenericPageableResponse(
            code = 500,
            message = this.message ?: this.javaClass.name
        )
    }
}