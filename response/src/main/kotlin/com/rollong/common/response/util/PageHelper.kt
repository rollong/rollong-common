package com.rollong.common.response.util

import com.github.pagehelper.Page
import com.github.pagehelper.PageInfo
import com.rollong.common.response.GenericPageableResponse

fun <T> Page<T>.toPageableResponse(): GenericPageableResponse<T> {
    return GenericPageableResponse(
        items = this.result.toList(),
        totalElements = this.total,
        totalPages = this.pages,
        currentPage = this.pageNum - 1,
        pageSize = this.pageSize
    )
}

fun <T> PageInfo<T>.toPageableResponse(): GenericPageableResponse<T> {
    return GenericPageableResponse(
        items = this.list,
        totalElements = this.total,
        totalPages = this.pages,
        currentPage = this.pageNum - 1,
        pageSize = this.pageSize
    )
}