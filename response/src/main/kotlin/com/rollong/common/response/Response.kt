package com.rollong.common.response

import com.rollong.common.exception.BaseException
import com.rollong.common.exception.ExceptionConstant

interface Response {
    var code: Int
    var errCode: String?
    var message: String
    fun onError(e: Throwable) {
        when (e) {
            is BaseException -> {
                this.code = e.code
                this.errCode = e.errCode
                this.message = e.message ?: e.javaClass.name
            }
            else -> {
                this.code = 500
                this.errCode = ExceptionConstant.FAIL
                this.message = e.message ?: e.javaClass.name
            }
        }
    }
}