package com.rollong.common.response

data class GenericPageableResponse<T> constructor(
    override var code: Int = 0,
    override var errCode: String? = null,
    override var message: String = "",
    var items: List<T> = listOf(),
    var totalPages: Int = 0,
    var totalElements: Long = 0,
    var currentPage: Int = 0,
    var pageSize: Int = 0
) : Response {
    constructor(code: Int? = 0, message: String, items: List<T>, totalPages: Int = 0, totalElements: Long = 0,
                currentPage: Int = 0, pageSize: Int = 0
    ) : this() {
        this.code = code ?: 0
        this.message = message
        this.items = items
        this.totalPages = totalPages
        this.totalElements = totalElements
        this.currentPage = currentPage
        this.pageSize = pageSize
    }
}