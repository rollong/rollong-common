package com.rollong.common.dto

import org.junit.Test

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-12 16:07
 * @project rollong-common
 * @filename DTOtest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common
 * @description
 */
class DTOtest {

    @Test
    fun hashMap() {
        val map: LinkedHashMap<String, Any?> = linkedMapOf(
            "a" to 123,
            "b" to 456,
            "d" to listOf("x1", "x2"),
            "e" to mapOf("a1" to "xx", "a2" to 123)
        )
        val target = ModelDataMapper.map(
            entity = map,
            outClass = Target::class.java
        )
        println(target)
        val map2 = ModelDataMapper.map(target, mutableMapOf<String, Any?>())
        println(map2)
    }
}

data class Target(
    var a: String? = null,
    var b: Int? = null,
    var c: String? = null,
    var d: Collection<String>? = null,
    var e: Pair? = null
)

data class Pair(
    var a1: String? = null,
    var a2: Long? = null
)