package com.rollong.common.dto

import com.rollong.common.response.GenericPageableResponse
import org.springframework.data.domain.Page

fun <P, V> page2GenericPageableResponse(page: Page<P>, outCLass: Class<V>): GenericPageableResponse<V> {
    val result = ModelDataMapper.mapAll(page.content, outCLass)
    return GenericPageableResponse(
        items = result,
        totalElements = page.totalElements,
        totalPages = page.totalPages,
        currentPage = page.pageable.pageNumber,
        pageSize = page.pageable.pageSize
    )
}