package com.rollong.common.dto.util

import com.rollong.common.dto.ModelDataMapper
import com.rollong.common.response.GenericPageableResponse
import com.rollong.common.response.GenericResponse


// convertToResponse
fun <V> Any?.convertToResponse(outCLass: Class<V>): GenericResponse<V> {
    return GenericResponse(
        items = ModelDataMapper.map(this, outCLass),
        code = 200,
        message = "OK"
    )
}

// convertToPageableResponse
fun <P, V> List<P>.convertToPageableResponse(outCLass: Class<V>): GenericPageableResponse<V> {
    val data = ModelDataMapper.mapAll(this, outCLass)
    return GenericPageableResponse(
        items = data,
        totalPages = if (data.isNotEmpty()) 1 else 0,
        totalElements = data.size.toLong(),
        currentPage = 0,
        pageSize = 0
    )
}