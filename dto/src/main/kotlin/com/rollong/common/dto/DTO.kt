package com.rollong.common.dto

import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.core.metadata.IPage
import com.baomidou.mybatisplus.extension.plugins.pagination.Page
import com.rollong.common.response.GenericPageableResponse
import com.rollong.common.response.GenericResponse
import org.modelmapper.AbstractCondition
import org.modelmapper.ModelMapper
import org.modelmapper.convention.MatchingStrategies

/**
 * Title：DTO
 * Description：
 * @author Flicker
 * @create 2020-08-03 17:19
 **/
abstract class DTO {
    @Transient
    @TableField(exist = false)
    //@ApiModelProperty(hidden = true)
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    open var nullProps: MutableList<String>? = null

    inline fun <reified T : Any> createModel(): T {
        val modelMapper = ModelMapper()
        modelMapper.configuration.matchingStrategy = MatchingStrategies.STRICT
        return modelMapper.map(this, T::class.java)
    }

    fun updateModel(model: Any) {
        val modelMapper = ModelMapper()
        modelMapper.configuration.matchingStrategy = MatchingStrategies.STRICT
        modelMapper.configuration.propertyCondition =
            SkipOrNullPropsCondition<Any, Any>(this.nullProps, mutableListOf())
        modelMapper.map(this, model)
    }

    fun updateModel(model: Any, skipProps: MutableList<String>) {
        val modelMapper = ModelMapper()
        modelMapper.configuration.matchingStrategy = MatchingStrategies.STRICT
        modelMapper.configuration.propertyCondition = SkipOrNullPropsCondition<Any, Any>(this.nullProps, skipProps)
        modelMapper.map(this, model)
    }
}

object ModelDataMapper {

    private var modelMapper = ModelMapper()

    init {
        modelMapper.configuration.matchingStrategy = MatchingStrategies.STRICT
    }

    fun <D, T> map(entity: T, outClass: Class<D>): D {
        return modelMapper.map(entity, outClass)
    }

    fun <S, D> map(source: S, destination: D): D {
        modelMapper.configuration.propertyCondition =
            SkipOrNullPropsCondition<Any, Any>(nullProps = null, skipProps = mutableListOf())
        modelMapper.map(source, destination)
        return destination
    }

    fun <S, D> map(
        entity: S,
        outClass: Class<D>,
        skipProperties: MutableList<String> = mutableListOf()
    ): D {
        modelMapper.configuration.propertyCondition =
            SkipOrNullPropsCondition<Any, Any>(nullProps = null, skipProps = skipProperties)
        return modelMapper.map(entity, outClass)
    }

    fun <S, D> map(
        source: S,
        destination: D,
        skipProperties: MutableList<String> = mutableListOf(),
        nullProps: MutableList<String>? = null
    ): D {
        modelMapper.configuration.propertyCondition = SkipOrNullPropsCondition<Any, Any>(
            nullProps = nullProps,
            skipProps = skipProperties
        )
        modelMapper.map(source, destination)
        return destination
    }

    //
    fun <S, D> map(
        source: S,
        destination: D,
        nullProps: MutableList<String>? = null
    ): D {
        modelMapper.configuration.propertyCondition = SkipOrNullPropsCondition<Any, Any>(nullProps, mutableListOf())
        modelMapper.map(source, destination)
        return destination
    }

    fun <D, T> mapAll(
        entityList: Collection<T>,
        outCLass: Class<D>
    ): List<D> {
        return entityList.map { entity: T -> map(entity, outCLass) }
    }

    fun <D, T> mapAll(
        entityList: Collection<T>,
        outClass: Class<D>,
        skipProperties: MutableList<String> = mutableListOf()
    ): List<D> {
        return entityList.map { entity: T ->
            map(
                entity = entity,
                outClass = outClass,
                skipProperties = skipProperties
            )
        }
    }

    fun <D, T> mapPage(
        pageData: Page<T>,
        outClass: Class<D>,
    ): Page<D> {
        val p = pageData.records
        val records = p.map { entity: T ->
            map(
                entity = entity,
                outClass = outClass
            )
        }
        return Page<D>().apply {
            this.records = records
            this.total = pageData.total
            this.size = pageData.size
            this.current = pageData.current
            this.orders = pageData.orders
        }
    }
}

class SkipOrNullPropsCondition<S, D>() : AbstractCondition<S, D>() {
    private lateinit var skipProps: MutableList<String>
    private var nullProps: MutableList<String>? = null

    constructor(nullProps: MutableList<String>?, skipProps: MutableList<String>) : this() {
        this.skipProps = skipProps
        this.nullProps = nullProps
    }

    override fun applies(context: org.modelmapper.spi.MappingContext<S, D>): Boolean {
        val field = context.mapping.destinationProperties[0].name
        if (skipProps.isNotEmpty() && skipProps.contains(field)) {
            val notContains = !skipProps.contains(field)
            val sourceIsNotNull = context.source != null
            return notContains && sourceIsNotNull
        }
        val nullProps = this.nullProps
        return context.source != null || nullProps != null && nullProps.contains(field)
    }
}


fun <P, V> Page<P>.convertToModel(outCLass: Class<V>): Page<V> {
    val data = ModelDataMapper.mapAll(this.records, outCLass)
    return Page<V>().apply {
        this.records = data
        this.total = this.total
        this.size = this.size
        this.current = this.current
        this.pages = this.pages
    }
}

// convertToResponse
fun <V> Any?.convertToResponse(outCLass: Class<V>): GenericResponse<V> {
    return GenericResponse(
        items = if (this != null) ModelDataMapper.map(this, outCLass) else null,
        code = 200,
        message = "OK"
    )
}

// convertToPageableResponse
fun <P, V> List<P>.convertToPageableResponse(outCLass: Class<V>): GenericPageableResponse<V> {
    val data = ModelDataMapper.mapAll(this, outCLass)
    return GenericPageableResponse<V>(
        items = data,
        totalPages = if (data.isNotEmpty()) 1 else 0,
        totalElements = data.size.toLong(),
        currentPage = 0,
        pageSize = 0
    )
}

fun <P, V> IPage<P>.convertToPageableResponse(outCLass: Class<V>): GenericPageableResponse<V> {
    val data = ModelDataMapper.mapAll(this.records, outCLass)
    return GenericPageableResponse<V>(
        items = data,
        totalPages = this.pages.toInt(),
        totalElements = this.total,
        currentPage = this.current.toInt(),
        pageSize = this.size.toInt()
    )
}

fun <P, V> GenericPageableResponse<P>.convertToPageableResponse(outCLass: Class<V>): GenericPageableResponse<V> {
    val data = ModelDataMapper.mapAll(this.items, outCLass)
    return GenericPageableResponse<V>(
        items = data,
        totalPages = this.totalPages,
        totalElements = this.totalElements,
        currentPage = this.currentPage,
        pageSize = this.pageSize
    )
}

fun <V> Any.convertToModel(outCLass: Class<V>): V {
    return ModelDataMapper.map(this, outCLass)
}

fun <P, V> List<P>.convertToModel(outCLass: Class<V>): List<V> {
    return ModelDataMapper.mapAll(this, outCLass)
}