package com.rollong.common.dto.util

import com.baomidou.mybatisplus.core.metadata.IPage
import com.baomidou.mybatisplus.extension.plugins.pagination.Page
import com.rollong.common.dto.ModelDataMapper
import com.rollong.common.response.GenericPageableResponse

fun <P, V> Page<P>.convertToModel(outCLass: Class<V>): Page<V> {
    val data = ModelDataMapper.mapAll(this.records, outCLass)
    val pageResult = Page<V>(
        this.current,
        this.size,
        this.total
    )
    pageResult.records = data
    pageResult.pages = this.pages
    return pageResult
}

fun <P, V> IPage<P>.convertToPageableResponse(outCLass: Class<V>): GenericPageableResponse<V> {
    val data = ModelDataMapper.mapAll(this.records, outCLass)
    return GenericPageableResponse<V>(
        items = data,
        totalPages = this.pages.toInt(),
        totalElements = this.total,
        currentPage = this.current.toInt() - 1,
        pageSize = this.size.toInt()
    )
}