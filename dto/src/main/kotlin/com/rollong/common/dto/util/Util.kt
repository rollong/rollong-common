package com.rollong.common.dto.util

import com.rollong.common.dto.ModelDataMapper

fun <V> Any.convertToModel(outCLass: Class<V>): V {
    return ModelDataMapper.map(this, outCLass)
}

fun <P, V> List<P>.convertToModel(outCLass: Class<V>): List<V> {
    return ModelDataMapper.mapAll(this, outCLass)
}