package com.rollong.common.storage.aliyun

import com.aliyun.oss.HttpMethod
import com.aliyun.oss.OSSClient
import com.aliyun.oss.common.auth.DefaultCredentialProvider
import com.aliyun.oss.model.*
import com.rollong.common.storage.StorageDisk
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 0:04
 * @project rollong-common
 * @filename OSSBucket.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.aliyun
 * @description
 */
class OSSBucket(
    private val config: OSSBucketConfig
) : StorageDisk {

    private fun client() = OSSClient(
        this.config.endpoint,
        DefaultCredentialProvider(this.config.accessKey, this.config.accessSecret),
        null
    )

    private fun fullpath(path: String) = (this.config.prefix.trim('/') + '/' + path.trim('/')).trimStart('/')

    override fun write(path: String, stream: InputStream) {
        val client = this.client()
        val request = PutObjectRequest(this.config.bucketName, this.fullpath(path), stream)
        try {
            client.putObject(request)
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    fun initiateMultipartUpload(objectName: String): String {
        val client = this.client()
        return try {
            val request = InitiateMultipartUploadRequest(this.config.bucketName, this.fullpath(objectName))
            client.initiateMultipartUpload(request).uploadId
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    /**
     * @param partNumber 每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出这个范围，OSS将返回InvalidArgument的错误码。
     * @param 设置分片大小。除了最后一个分片没有大小限制，其他的分片最小为100 KB
     */
    fun multipartUpload(
        uploadId: String,
        path: String,
        partNumber: Int,
        partSize: Long,
        inputStream: InputStream
    ): PartETag {
        val client = this.client()
        return try {
            val request = UploadPartRequest()
            request.bucketName = this.config.bucketName
            request.key = this.fullpath(path)
            request.uploadId = uploadId
            request.inputStream = inputStream
            request.partSize = partSize
            request.partNumber = partNumber
            val result = client.uploadPart(request)
            result.partETag
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    fun completeMultipartUpload(uploadId: String, path: String, eTags: List<PartETag>) {
        val client = this.client()
        try {
            val request = CompleteMultipartUploadRequest(this.config.bucketName, this.fullpath(path), uploadId, eTags)
            client.completeMultipartUpload(request)
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    override fun publicURL(path: String, style: String?, timeout: Long, timeUnit: TimeUnit): String {
        if (this.config.publicAcl) {
            return this.config.publicURL.trimEnd('/') + '/' + this.fullpath(path)
        } else {
            val req = GeneratePresignedUrlRequest(
                this.config.bucketName,
                this.fullpath(path),
                HttpMethod.GET
            )
            req.expiration = Date(System.currentTimeMillis() + timeUnit.toMillis(timeout))
            if (null != style) {
                req.process = style
            }
            val url = this.client().generatePresignedUrl(req).toExternalForm()
            return this.config.publicURL.trimEnd('/') + '/' + url.substring(url.indexOf('/', 8)).trimStart('/')
        }
    }

    override fun read(path: String): InputStream {
        val client = this.client()
        val request = GetObjectRequest(this.config.bucketName, this.fullpath(path))
        try {
            val ossObject = client.getObject(request)
            try {
                val length = ossObject.objectMetadata.contentLength.toInt()
                return if (length > 0) {
                    ByteArrayInputStream(ossObject.objectContent.readBytes())
                } else {
                    ByteArrayInputStream(ossObject.objectContent.readBytes())
                }
            } catch (e: Throwable) {
                throw e
            } finally {
                ossObject.close()
            }
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    override fun delete(path: String) {
        val client = this.client()
        try {
            client.deleteObject(this.config.bucketName, path)
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    override fun exists(filename: String): Boolean {
        val client = this.client()
        try {
            return client.doesObjectExist(this.config.bucketName, this.fullpath(filename), true)
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }

    override fun list(path: String, limit: Int?): List<StorageDisk.FileData> {
        val client = this.client()
        val request = ListObjectsRequest()
        if (path.isNotEmpty()) {
            request.prefix = path
        }
        limit?.let { request.maxKeys = it }
        request.bucketName = this.config.bucketName
        try {
            val res = client.listObjects(request)
            return res.objectSummaries.map {
                StorageDisk.FileData(
                    name = it.key,
                    createdAt = it.lastModified.time
                )
            }
        } catch (e: Throwable) {
            throw e
        } finally {
            client.shutdown()
        }
    }
}