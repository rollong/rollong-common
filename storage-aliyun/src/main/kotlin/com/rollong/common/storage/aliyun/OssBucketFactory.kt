package com.rollong.common.storage.aliyun

import com.rollong.common.storage.StorageDisk
import com.rollong.common.storage.factory.StorageDiskFactory

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/22 17:07
 * @project rollong-common
 * @filename OssBucketFactory.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.aliyun
 * @description
 */
class OssBucketFactory : StorageDiskFactory {

    override val name = "oss"

    override fun make(config: Map<String, String>): StorageDisk {
        return OSSBucket(
            config = OSSBucketConfig(
                endpoint = config["endpoint"] ?: throw RuntimeException("未指定endpoint"),
                accessKey = config["accessKey"] ?: throw RuntimeException("未指定accessKey"),
                accessSecret = config["accessSecret"] ?: throw RuntimeException("未指定accessSecret"),
                bucketName = config["bucketName"] ?: throw RuntimeException("未指定bucketName"),
                publicAcl = (config["publicAcl"]?.toBoolean()) ?: false,
                publicURL = config["publicURL"] ?: throw RuntimeException("未指定publicURL"),
                prefix = config["prefix"] ?: ""
            )
        )
    }
}