package com.rollong.common.json

import org.junit.Test
import java.sql.Timestamp
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-12 15:34
 * @project rollong-common
 * @filename JsonTest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.json
 * @description
 */
data class DataObject(
    var a: Long? = null,
    var b: String = ""
)

class JsonTest {

    @Test
    fun test() {
        val json = "{\n" +
            "\t\"a\":1,\n" +
            "\t\"b\":2,\n" +
            "\t\"c\":3\n" +
            "}"
        val xx = JSONUtil.parseObject(json, DataObject::class.java)
        val xx2 = JSONUtil.parseObject<DataObject>(json)
        println(JSONUtil.toJSONString(xx))
    }

    @Test
    fun date() {
        val data = Data(
            ts = Timestamp(2000L),
            s = "string123",
            l = 3L
        )
        val json = JSONUtil.toJSONString(data)
        val data2 = JSONUtil.parseObject<Data>(json)
        println(json)
    }
}

data class Data(
    var ts: Date? = null,
    var s: String? = null,
    var l: Long? = null
)