package com.rollong.common.json

import com.fasterxml.jackson.core.type.TypeReference

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/02 23:00
 * @project rollong-common
 * @filename Helper.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.json
 * @description
 */

inline fun <reified T : Any> String.parseJsonObject(): T {
    val reference = object : TypeReference<T>() {}
    return JSONUtil.objectMapper.readValue(this, reference)
}

inline fun <reified T : Any> String.parseJsonArray(): List<T> {
    val reference = object : TypeReference<List<T>>() {}
    return JSONUtil.objectMapper.readValue(this, reference)
}