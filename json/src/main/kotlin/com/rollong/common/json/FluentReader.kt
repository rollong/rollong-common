package com.rollong.common.json

import java.io.InputStream
import java.math.BigDecimal
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 13:50
 * @project rollong-common
 * @filename FluentReader.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.json
 * @description
 */
open class FluentReader {

    private val data: Map<String, Any?>

    constructor(body: String) {
        this.data = JSONUtil.parseObject(body)
    }

    constructor(body: InputStream) {
        this.data = JSONUtil.parseObject(body)
    }

    constructor(body: ByteArray) {
        this.data = JSONUtil.parseObject(body)
    }

    constructor(map: Map<String, Any?>) {
        this.data = map
    }

    fun hasKey(key: String) = this.data.containsKey(key)

    fun get(key: String): Any? = this.data[key]

    fun getString(key: String) = this.data[key]?.let {
        when (it) {
            is String -> it
            else -> it.toString()
        }
    }

    fun getLong(key: String) = this.data[key]?.let {
        when (it) {
            is Long -> it
            is Int -> it.toLong()
            is String -> it.toLong()
            is Date -> it.time
            else -> null
        }
    }

    fun getBoolean(key: String) = this.data[key] as Boolean?

    fun getDecimal(key: String) = this.data[key]?.let {
        when (it) {
            is BigDecimal -> it
            is Int -> it.toBigDecimal()
            is Long -> it.toBigDecimal()
            is Float -> it.toBigDecimal()
            is Double -> it.toBigDecimal()
            is String -> it.toBigDecimal()
            else -> null
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getList(key: String, @Suppress("UNUSED_PARAMETER") clazz: Class<T>) = this.data[key] as List<T>?

    @Suppress("UNCHECKED_CAST")
    fun getList(key: String): List<FluentReader>? {
        val result = this.data[key] as List<Map<String, Any?>>?
        return result?.map { FluentReader(it) }
    }

    override fun toString(): String {
        return JSONUtil.toJSONString(this.data)
    }
}