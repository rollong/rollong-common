package com.rollong.common.json

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.io.InputStream

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 9:00
 * @project rollong-common
 * @filename JSONUtil.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.json
 * @description JSON工具类
 */
object JSONUtil {

    //see https://github.com/FasterXML/jackson-module-kotlin
    val objectMapper: ObjectMapper by lazy {
        this.mapper()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true)
    }

    fun mapper() = ObjectMapper().registerKotlinModule()

    fun toJSONString(obj: Any): String {
        return this.objectMapper.writeValueAsString(obj)
    }

    inline fun <reified T : Any> parseObject(src: String): T {
        val reference = object : TypeReference<T>() {}
        return this.objectMapper.readValue(src, reference)
    }

    inline fun <reified T : Any> parseObject(src: InputStream): T {
        val reference = object : TypeReference<T>() {}
        return this.objectMapper.readValue(src, reference)
    }

    inline fun <reified T : Any> parseObject(src: ByteArray): T {
        val reference = object : TypeReference<T>() {}
        return this.objectMapper.readValue(src, reference)
    }

    @Suppress("UNUSED_PARAMETER")
    fun <T : Any> parseObject(src: String, clazz: Class<T>): T {
        return this.objectMapper.readValue(src, clazz)
    }

    fun <T : Any> parseObject(src: InputStream, clazz: Class<T>): T {
        return this.objectMapper.readValue(src, clazz)
    }

    fun <T : Any> parseObject(src: ByteArray, clazz: Class<T>): T {
        return this.objectMapper.readValue(src, clazz)
    }

    inline fun <reified T : Any> parseArray(src: String): List<T?> {
        val reference = object : TypeReference<List<T?>>() {}
        return this.objectMapper.readValue(src, reference)
    }

    inline fun <reified T : Any> parseArray(src: InputStream): List<T?> {
        val reference = object : TypeReference<List<T?>>() {}
        return this.objectMapper.readValue(src, reference)
    }

    inline fun <reified T : Any> parseArray(src: ByteArray): List<T?> {
        val reference = object : TypeReference<List<T?>>() {}
        return this.objectMapper.readValue(src, reference)
    }

    @Suppress("UNUSED_PARAMETER")
    fun <T : Any> parseArray(src: String, clazz: Class<T>): List<T?> {
        val reference = object : TypeReference<List<T?>>() {}
        return this.objectMapper.readValue(src, reference)
    }

    @Suppress("UNUSED_PARAMETER")
    fun <T : Any> parseArray(src: InputStream, clazz: Class<T>): List<T?> {
        val reference = object : TypeReference<List<T?>>() {}
        return this.objectMapper.readValue(src, reference)
    }

    @Suppress("UNUSED_PARAMETER")
    fun <T : Any> parseArray(src: ByteArray, clazz: Class<T>): List<T?> {
        val reference = object : TypeReference<List<T?>>() {}
        return this.objectMapper.readValue(src, reference)
    }

}