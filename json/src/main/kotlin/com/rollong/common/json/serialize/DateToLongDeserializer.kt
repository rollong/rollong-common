package com.rollong.common.json.serialize

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/15 12:57
 * @project rollong-common
 * @filename DateToLongDeserializer.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.json.serialize
 * @description
 */
class DateToLongDeserializer:JsonDeserializer<Long>() {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Long {
        return (p.currentValue as Date).time
    }
}