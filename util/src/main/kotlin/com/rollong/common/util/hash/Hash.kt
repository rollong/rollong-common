package com.rollong.common.util.hash

import java.nio.charset.Charset
import java.security.MessageDigest

/**
 * Supported algorithms on Android:
 *
 * Algorithm	Supported API Levels
 * MD5          1+
 * SHA-1	    1+
 * SHA-224	    1-8,22+
 * SHA-256	    1+
 * SHA-384	    1+
 * SHA-512	    1+
 */

fun String.digest(type: String, charset: Charset = Charsets.UTF_8): ByteArray =
    this.toByteArray(charset = charset).digest(type = type)

fun ByteArray.digest(type: String) = MessageDigest
    .getInstance(type)
    .digest(this)

fun ByteArray.hex(): String {
    val hexChars = "0123456789ABCDEF"
    val result = StringBuilder(this.size * 2)
    this.forEach {
        val i = it.toInt()
        result.append(hexChars[i shr 4 and 0x0f])
        result.append(hexChars[i and 0x0f])
    }
    return result.toString()
}

fun ByteArray.sha512() = this.digest("SHA-512")

fun ByteArray.sha256() = this.digest("SHA-256")

fun ByteArray.sha1() = this.digest("SHA-1")

fun ByteArray.md5() = this.digest("MD5")

fun String.sha512() = this.digest("SHA-512")

fun String.sha256() = this.digest("SHA-256")

fun String.sha1() = this.digest("SHA-1")

fun String.md5() = this.digest("MD5")

fun String.within(length: Int) =
    when {
        this.length <= length -> this
        length > 0 -> this.substring(0, length)
        else -> ""
    }