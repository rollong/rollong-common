package com.rollong.common.util.string

fun String?.like() = if (null == this) null else "%${this}%"