package com.rollong.common.util.collection

import java.util.stream.Stream

fun Iterable<String>.implode(glue: String = ","): String {
    val sb = StringBuffer()
    this.forEach {
        if (sb.isEmpty()) {
            sb.append(it)
        } else {
            sb.append("$glue$it")
        }
    }
    return sb.toString()
}

fun Array<String>.implode(glue: String = ","): String {
    val sb = StringBuffer()
    this.forEach {
        if (sb.isEmpty()) {
            sb.append(it)
        } else {
            sb.append("$glue$it")
        }
    }
    return sb.toString()
}

inline fun Int.repeats(something: (Int) -> Any) {
    for (i in 0.until(this)) {
        something(i)
    }
}

inline fun <E, R> Stream<E>.batch(
    size: Int,
    crossinline action: (index: Int, items: List<E>) -> Collection<R>
): Stream<R> {
    val buffer = mutableListOf<E>()
    val results = mutableListOf<R>()
    var index = 0
    this.forEachOrdered {
        if (buffer.size < size) {
            buffer.add(it)
        } else {
            results.addAll(action.invoke(index++, buffer))
            buffer.clear()
        }
    }
    if (buffer.isNotEmpty()) {
        results.addAll(action.invoke(index, buffer))
    }
    return results.stream()
}