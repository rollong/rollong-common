package com.rollong.common.util.collection

import com.github.pagehelper.Page

inline fun <T : Any, R : Any> Page<T>.mapPage(crossinline transform: (T) -> R): Page<R> {
    val page = Page<R>(this.pageNum, this.pageSize)
    page.total = this.total
    page.startRow = this.startRow
    page.endRow = this.endRow
    page.pages = this.pages
    this.forEach {
        page.add(transform(it))
    }
    return page
}