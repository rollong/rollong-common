package com.rollong.common.util.encoding

import java.nio.charset.Charset

fun String.code(
    to: Charset,
    from: Charset = Charsets.UTF_8
) = String(this.toByteArray(from), to)


fun Char.isEnglish(): Boolean = this.toInt().let {
    (0x41..0x5A).contains(it) || (0x61..0x7A).contains(it)
}

fun Char.isAsciiNonControl(): Boolean = this.toInt().let {
    (0x20..0x7E).contains(it)
}

fun Char.isChinese(): Boolean = this.toInt().let {
    (0x4E00..0x9FA5).contains(it) //基本汉字	20902字	4E00-9FA5
        || (0x9FA6..0x9FEF).contains(it) //基本汉字补充	74字	9FA6-9FEF
        || (0x3400..0x4DB5).contains(it)//扩展A	6582字	3400-4DB5
        || (0x20000..0x2A6D6).contains(it)//扩展B	42711字	20000-2A6D6
        || (0x2A700..0x2B734).contains(it)//扩展C	4149字	2A700-2B734
        || (0x2B740..0x2B81D).contains(it)//扩展D	222字	2B740-2B81D
        || (0x2B820..0x2CEA1).contains(it)//扩展E	5762字	2B820-2CEA1
        || (0x2CEB0..0x2EBE0).contains(it)//扩展F	7473字	2CEB0-2EBE0
}

fun String.filterSpecial(
    emoji: Boolean = false,
    ascii: Boolean = true,
    chinese: Boolean = true
): String {
    val sb = StringBuffer()
    this.toCharArray().filter {
        (emoji && EmojiUtil.isEmoji(it))
            || (ascii && it.isAsciiNonControl())
            || (chinese && it.isChinese())
    }.forEach {
        sb.append(it)
    }
    return sb.toString()
}

fun String.maxLength(size: Int) = if (this.length <= size) this else {
    val sb = StringBuffer()
    var count = 0
    var emojiCharsCount = 0
    for (char in this.toCharArray()) {
        //一个emoji = 2个Char = 4Byte
        if (char.isEmoji()) {
            emojiCharsCount++
        }
        sb.append(char)
        if (++count >= size && emojiCharsCount % 2 == 0) {
            break
        }
    }
    sb.toString()
}

fun String?.notBlankOrNull() = if (null == this || this.isBlank()) {
    null
} else {
    this
}