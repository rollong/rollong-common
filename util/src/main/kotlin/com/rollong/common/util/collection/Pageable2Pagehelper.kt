package com.rollong.common.util.collection

import com.github.pagehelper.PageHelper
import org.springframework.data.domain.Pageable

fun <E> Pageable.toPageHelper() = PageHelper.startPage<E>(this.pageNumber + 1, this.pageSize)