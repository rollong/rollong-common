package com.rollong.common.util.displayName

interface HasDisplayName {
    val displayName: String
}