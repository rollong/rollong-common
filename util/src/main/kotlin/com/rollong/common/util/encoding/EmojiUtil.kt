package com.rollong.common.util.encoding

object EmojiUtil {

    private val emojiSoftBankTable: Set<Int>

    init {
        var chars = mutableListOf<Int>()
        listOf(
            (0xe001..0xe05a),
            (0xe101..0xe15a),
            (0xe201..0xe253),
            (0xe301..0xe34d),
            (0xe401..0xe44c),
            (0xe501..0xe537)
        ).map {
            chars.addAll(it)
        }
        chars.addAll(
            listOf(
                0x21d4, 0x25b2, 0x25bc, 0x25cb, 0x25cf, 0x3013, 0xff5e, 0xffe5
            )
        )
        this.emojiSoftBankTable = chars.toSet()
    }

    fun isEmoji(c: Char) = this.emojiSoftBankTable.contains(c.toInt())
}

fun Char.isEmoji() = EmojiUtil.isEmoji(this)

//fun Char.isEmoji() = !this.toInt().let {
//    (it == 0x0)
//            || (it == 0x9)
//            || (it == 0xA)
//            || (it == 0xD)
//            || ((it >= 0x20) && (it <= 0xD7FF))
//            || ((it >= 0xE000) && (it <= 0xFFFD))
//            || ((it >= 0x10000) && (it <= 0x10FFFF))
//}