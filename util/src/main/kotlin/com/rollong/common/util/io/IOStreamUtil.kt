package com.rollong.common.util.io

import java.io.ByteArrayOutputStream
import java.io.InputStream

fun InputStream.readAllToByteArray(): ByteArray {
    val result = ByteArrayOutputStream()
    val buffer = ByteArray(1024)
    while (true) {
        val length = this.read(buffer)
        if (-1 != length) {
            result.write(buffer, 0, length)
        } else {
            return result.toByteArray()
        }
    }
}