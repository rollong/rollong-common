package com.rollong.common.util.encoding

import java.nio.charset.Charset
import java.util.*

fun ByteArray.base64encode() = Base64.getEncoder().encodeToString(this)

fun String.base64encode(charset: Charset = Charsets.UTF_8) = this.toByteArray(charset).base64encode()

fun String.base64decode() = Base64.getDecoder().decode(this)


/**
 * 用于url的base64encode
 * '+' => '*', '/' => '-', '=' => '_'
 * @param data 需要编码的数据
 * @return 编码后的base64数据
 */
fun String.urlBase64encode(charset: Charset = Charsets.UTF_8) = this.base64encode(charset).let {
    val base64String = it
    val sb = StringBuffer()
    base64String.toCharArray().forEach {
        val char = when (it) {
            '+' -> '*'
            '/' -> '-'
            '=' -> '_'
            else -> it
        }
        sb.append(char)
    }
    sb.toString()
}

/**
 * 用于url的base64decode
 * '*' => '+', '-' => '/', '_' => '='
 * @param data 需要解码的数据
 * @return 解码后的数据
 */
fun String.urlBase64decode() = this.let {
    val urlBase64String = it
    val sb = StringBuffer()
    urlBase64String.toCharArray().forEach {
        val char = when (it) {
            '*' -> '+'
            '-' -> '/'
            '_' -> '='
            else -> it
        }
        sb.append(char)
    }
    sb.toString()
}