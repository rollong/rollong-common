package com.rollong.common.util.idcard

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * 身份证号码规则：<br></br>
 * 第一代身份证：<br></br>
 * 1-2：所在省（直辖市、自治区）的代码<br></br>
 * 3-4：所在地级市（自治州）的代码<br></br>
 * 5-6：所在区（县、自治县、县级市）的代码<br></br>
 * 7-12：出生年（两位）、月、日<br></br>
 * 13-14：所在地的派出所的代码<br></br>
 * 15：奇数表示男性，偶数表示女性<br></br>
 * 第二代身份证：<br></br>
 * 1-2：所在省（直辖市、自治区）的代码<br></br>
 * 3-4：所在地级市（自治州）的代码<br></br>
 * 5-6：所在区（县、自治县、县级市）的代码<br></br>
 * 7-14：出生年、月、日<br></br>
 * 15-16：所在地的派出所的代码<br></br>
 * 17：奇数表示男性，偶数表示女性<br></br>
 * 18：也有的说是个人信息码，不是随计算机的随机产生，它是 用来检验身份证的正确性。校检码可以是0-9的数字，有时也用X表示。
 *
 * @author jianggujin
 */
object IdCardUtil {

    private val PARTTERN_CARD_NO = Pattern.compile("\\d{15}|\\d{17}[0-9X]")
    private val PARTTERN_DATE = Pattern.compile(
        "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))0229)"
    )

    // 1-17位相乘因子数组
    private val FACTOR = intArrayOf(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2)

    // 18位随机码数组
    private val RANDOM = "10X98765432".toCharArray()

    private val df = SimpleDateFormat("yyyyMMdd")

    fun validate(idCardNo: String): Boolean {
        // 对身份证号进行长度等简单判断
        if (!PARTTERN_CARD_NO.matcher(idCardNo).matches()) {
            return false
        }
        // 一代身份证
        if (15 == idCardNo.length) {
            return PARTTERN_DATE.matcher("19" + idCardNo.substring(6, 12)).matches()
        }
        // 二代身份证
        return if (18 == idCardNo.length && PARTTERN_DATE.matcher(idCardNo.substring(6, 14)).matches()) {
            // 判断随机码是否相等
            calculateRandom(idCardNo) == idCardNo[17]
        } else {
            false
        }
    }

    /**
     * 根据地域(6位数字)fake身份证号码
     * 123456(地域)78901234(生日)567(随机)8(校验)
     */
    fun fake(regionCode: String, birthdate: Date, male: Boolean = true): String {
        val random = if (male) {
            (Random().nextInt(500) * 2 + 1) % 1000
        } else {
            (Random().nextInt(500) * 2)
        }
        val idCardNo = regionCode + df.format(birthdate) + String.format("%03d", random)
        return idCardNo + calculateRandom(idCardNo)
    }

    /**
     * 计算最后一位随机码
     *
     * @param idCardNo
     * @return
     */
    private fun calculateRandom(idCardNo: String): Char {
        // 计算1-17位与相应因子乘积之和
        var total = 0
        for (i in 0..16) {
            total += Character.getNumericValue(idCardNo[i]) * FACTOR[i]
        }
        // 判断随机码是否相等
        return RANDOM[total % 11]
    }

    /**
     * 计算最后一位随机码
     *
     * @param idCardNo
     * @return
     */
    private fun calculateRandom(idCardNo: CharArray): Char {
        // 计算1-17位与相应因子乘积之和
        var total = 0
        for (i in 0..16) {
            total += Character.getNumericValue(idCardNo[i]) * FACTOR[i]
        }
        // 判断随机码是否相等
        return RANDOM[total % 11]
    }

    /**
     * 15和18位身份证号码转换
     *
     * @param idCardNo
     * @return
     */
    fun convert(idCardNo: String): String {
        if (!validate(idCardNo)) {
            throw IllegalArgumentException("idCardNo invalid.")
        }
        val result: CharArray
        var index = 0
        return if (idCardNo.length == 15) {// 添加年份与随机码
            result = CharArray(18)
            // 复制行政区域代码
            while (index < 6) {
                result[index] = idCardNo[index]
                index++
            }
            // 添加年份，固定值：19
            result[index++] = '1'
            result[index++] = '9'
            // 添加年月日与附加信息
            while (index < 17) {
                result[index] = idCardNo[index - 2]
                index++
            }
            result[index] = calculateRandom(result)
            String(result)
        } else if (idCardNo.length == 18) {// 去除年份与随机码
            result = CharArray(15)
            // 复制行政区域代码
            while (index < 6) {
                result[index] = idCardNo[index]
                index++
            }
            // 跳过两位年份；
            index += 2
            // 去除最后一位
            while (index < 17) {
                result[index - 2] = idCardNo[index]
                index++
            }
            String(result)
        } else {
            throw IllegalArgumentException("idCardNo length must equals 15 or 18.")
        }
    }
}
