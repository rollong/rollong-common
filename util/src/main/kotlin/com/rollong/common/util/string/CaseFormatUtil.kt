package com.rollong.common.util.string

import com.google.common.base.CaseFormat

/**
 * 驼峰转下划线
 */
fun String.toUnderLine(): String? {
    return CaseFormat.LOWER_CAMEL.converterTo(CaseFormat.LOWER_UNDERSCORE).convert(this)
}

/**
 * 下划线转驼峰
 */
fun String.toCamel(): String? {
    return CaseFormat.LOWER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL).convert(this)
}