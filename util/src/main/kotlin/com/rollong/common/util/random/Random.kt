package com.rollong.common.util.random

import com.rollong.common.util.collection.repeats
import java.util.*

fun Int.dice(total: Int = 100) = Random().nextInt(total) < this

fun IntRange.anyone() = Random().nextInt(this.endInclusive + 1 - this.start) + this.start

fun <E> List<E>.anyone(): E? {
    if (this.isEmpty()) {
        return null
    } else {
        return this.get(Random().nextInt(this.size))
    }
}

fun <E> List<E>.randomPick(count: Int = 1): List<E> {
    if (count <= 0) {
        return listOf()
    }
    if (count >= this.size) {
        return this
    }
    val idList = (0.until(this.size)).toMutableList()
    val results: MutableList<E> = mutableListOf()
    count.repeats {
        val id = idList.anyone()!!
        results.add(this[id])
        idList.remove(id)
    }
    return results
}

fun CharArray.toRandomString(length: Int): String {
    val sb = StringBuffer()
    val random = Random()
    length.repeats {
        sb.append(this.get(random.nextInt(this.size)))
    }
    return sb.toString()
}