package com.rollong.common.util.url

import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.Charset
import javax.servlet.http.HttpServletRequest

fun String.urlencode(charset: Charset = Charsets.UTF_8) = URLEncoder.encode(this, charset.name())

//java 10
//fun String.urldecode(charset: Charset = Charsets.UTF_8) = URLDecoder.decode(this, charset)
//java 8
fun String.urldecode(charset: Charset = Charsets.UTF_8) = URLDecoder.decode(this, charset.name())

fun String.baseURL(uri: String = ""): String? {
    return Regex("^(http:|https:|)//[^/?#]+(/|$|\\?|#)", RegexOption.IGNORE_CASE).find(this)?.value?.let {
        it.trimEnd('/') + '/' + uri.trimStart('/')
    }
}

fun HttpServletRequest.baseURL(
    uri: String = "",
    params: Map<String, String>? = null
): String {
    val url = this.requestURL.toString().baseURL(uri = uri)
    val p = if (null != params) {
        "?${params.urlParams()}"
    } else {
        ""
    }
    return url + p
}

fun Map<String, String>.urlParams(): String {
    val sb = StringBuffer()
    this.forEach {
        if (sb.isNotEmpty()) {
            sb.append('&')
        }
        sb.append("${it.key}=${it.value.urlencode()}")
    }
    return sb.toString()
}