#!/bin/bash
##首先补全aliyun-packages.settings.gradle

./gradlew :clean \
:dto:clean dto:build dto:uploadArchives \
:excel:clean excel:build excel:uploadArchives \
:exception:clean exception:build exception:uploadArchives \
:ioc-spring:clean ioc-spring:build ioc-spring:uploadArchives \
:json:clean json:build json:uploadArchives \
:mail:clean mail:build mail:uploadArchives \
:net:clean net:build net:uploadArchives \
:qrcode:clean qrcode:build qrcode:uploadArchives \
:response:clean response:build response:uploadArchives \
:snowflake:clean snowflake:build snowflake:uploadArchives \
:sql:clean sql:build sql:uploadArchives \
:storage:clean storage:build storage:uploadArchives \
:storage-aliyun:clean storage-aliyun:build storage-aliyun:uploadArchives \
:time:clean time:build time:uploadArchives \
:util:clean util:build util:uploadArchives \
:zip:clean zip:build zip:uploadArchives \
-x test \
-PuploadAliyun=true
