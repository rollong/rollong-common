package com.rollong.common.excel

import com.rollong.common.excel.data.ExcelColumn
import com.rollong.common.excel.data.ExcelDateFormat
import com.rollong.common.excel.data.toData
import com.rollong.common.excel.data.toExcelSheet
import com.rollong.common.excel.export.toFile
import com.rollong.common.excel.import.ExcelImportUtil
import org.junit.Test
import java.io.ByteArrayInputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 21:37
 * @project rollong-common
 * @filename ExcelTest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.excel
 * @description
 */
class ExcelTest {

    @Test
    fun testExport() {
        val df = SimpleDateFormat("yyyy-MM-dd")
        val records = listOf(
            Record(21, "张三", "三麻子", df.parse("1997-01-02"), "123456789012345678", 78),
            Record(31, "李四", "4麻子", df.parse("1997-02-04"), "123456789012345678", 12),
            Record(42, "王五", "5麻子", df.parse("1997-03-06"), "123456789012345678", 345),
            Record(442, "马六", "6麻子", df.parse("1997-04-08"), "123456789012345678", 45),
            Record(12, "冯七", "7麻子", df.parse("1997-05-10"), "123456789012345678", 718),
            Record(3, "赵八", "8麻子", df.parse("1997-06-12"), "123456789012345678", -33)
        )
        val file = File("records.xlsx")
        records.toExcelSheet().toFile(file)
    }

    @Test
    fun testImport() {
        val file = this.javaClass.classLoader.getResourceAsStream("records.xlsx")
        val records: List<Record> = ExcelImportUtil.import(ByteArrayInputStream(file.readAllBytes())).first().toData()
        println(records)
    }
}


data class Record(
    @ExcelColumn(order = 1, title = "id")
    var id: Int = 1,
    @ExcelColumn(order = 3, title = "真实姓名")
    var realname: String = "",
    @ExcelColumn(order = 2, title = "昵称")
    var nickname: String = "",
    @ExcelColumn(order = 4, title = "生日")
    @ExcelDateFormat(pattern = "yyyy/MM/dd")
    var birthday: Date = Date(),
    @ExcelColumn(order = 8, title = "身份证号码")
    var idCard: String = "",
    @ExcelColumn(order = 5, title = "分数")
    var score: Int = 0
)