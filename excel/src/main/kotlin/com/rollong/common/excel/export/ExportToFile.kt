package com.rollong.common.excel.export

import com.rollong.common.excel.data.SheetData
import java.io.File

fun SheetData.toFile(
    file: File
) {
    ExcelExportUtil.write(
        sheet = this,
        outputStream = file.outputStream()
    )
}