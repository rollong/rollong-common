package com.rollong.common.excel.export

import com.rollong.common.excel.data.SheetData
import org.apache.poi.ss.usermodel.RichTextString
import org.apache.poi.ss.util.CellRangeAddressList
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.OutputStream
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 21:17
 * @project rollong-common
 * @filename ExcelExportUtil.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.excel.export
 * @description Excel导出工具类
 */
object ExcelExportUtil {

    fun write(
        sheet: SheetData,
        outputStream: OutputStream
    ) = this.write(
        sheets = listOf(sheet),
        outputStream = outputStream
    )

    fun write(
        sheets: List<SheetData>,
        outputStream: OutputStream
    ) {
        val wk = XSSFWorkbook()
        try {
            sheets.forEach {
                val sheet = if (null != it.name) {
                    wk.createSheet(it.name)
                } else {
                    wk.createSheet()
                }
                this.fillSheet(
                    workbook = wk,
                    sheet = sheet,
                    sheetData = it
                )
            }
            wk.write(outputStream)
        } catch (e: Throwable) {
            throw e
        } finally {
            wk.close()
        }
    }

    private fun fillSheet(
        workbook: XSSFWorkbook,
        sheet: XSSFSheet,
        sheetData: SheetData
    ) {
        this.fillRow(sheet.createRow(0), sheetData.index)
        sheetData.index.cells.forEachIndexed { index, cell ->
            cell.width?.let {
                sheet.setColumnWidth(index, it)
            }
            cell.explicitList?.let {
                this.explicitConstraint(
                    workbook = workbook,
                    sheet = sheet,
                    menuItems = it,
                    column = index
                )
            }
        }
        sheetData.rows.forEachIndexed { index, row ->
            this.fillRow(
                row = sheet.createRow(index + 1),
                data = row
            )
        }
    }

    private fun explicitConstraint(
        sheet: XSSFSheet,
        menuItems: Array<String>,
        column: Int
    ) {
        val addressList = CellRangeAddressList(1, 65535, column, column)
        val helper = XSSFDataValidationHelper(sheet)
        val dvConstraint = helper.createExplicitListConstraint(menuItems)
        val dataValidation = helper.createValidation(dvConstraint, addressList)
        sheet.addValidationData(dataValidation)
    }

    /**
     * @link https://blog.csdn.net/hjnth/article/details/79121650
     * 通过隐藏sheet关联的方式建立约束
     */
    private fun explicitConstraint(
        workbook: XSSFWorkbook,
        sheet: XSSFSheet,
        menuItems: Array<String>,
        column: Int
    ) {
        //必须以字母开头，最长为31位
        val hiddenSheetName = "a" + UUID.randomUUID().toString().replace("-", "").substring(1, 31)
        //excel中的"名称"，用于标记隐藏sheet中的用作菜单下拉项的所有单元格
        val formulaId = "form" + UUID.randomUUID().toString().replace("-", "")
        val hiddenSheet = workbook.createSheet(hiddenSheetName)//用于存储 下拉菜单数据
        //存储下拉菜单项的sheet页不显示
        workbook.setSheetHidden(workbook.getSheetIndex(hiddenSheet), true)

        //隐藏sheet中添加菜单数据
        for (i in 0 until menuItems.size) {
            val row = hiddenSheet.createRow(i)
            //隐藏表的数据列必须和添加下拉菜单的列序号相同，否则不能显示下拉菜单
            val cell = row!!.createCell(column)
            cell!!.setCellValue(menuItems[i])
        }

        val namedCell = workbook.createName()//创建"名称"标签，用于链接
        namedCell.nameName = formulaId
        namedCell.refersToFormula = hiddenSheetName + "!A$1:A$" + menuItems.size
        val dvHelper = XSSFDataValidationHelper(sheet)
        val dvConstraint = dvHelper.createFormulaListConstraint(formulaId)

        val addressList = CellRangeAddressList(0, 65535, column, column)
        val validation = dvHelper.createValidation(dvConstraint, addressList)  //添加菜单(将单元格与"名称"建立关联)
        sheet.addValidationData(validation)
    }

    private fun fillRow(
        row: XSSFRow,
        data: SheetData.Row
    ) {
        data.style?.let {
            row.rowStyle = it
        }
        data.cells.forEachIndexed { index, cell ->
            val c = row.createCell(index)
            cell.style?.let {
                c.cellStyle = cell.style
            }
            val value = cell.value
            when (value) {
                is String? -> c.setCellValue(value)
                is RichTextString? -> c.setCellValue(value)
                is Date -> c.setCellValue(value)
                is Calendar -> c.setCellValue(value)
                is Int -> c.setCellValue(value.toString())
                is Long -> c.setCellValue(value.toString())
                is UUID -> c.setCellValue(value.toString())
                is Double -> c.setCellValue(value)
                is Float -> c.setCellValue(value.toDouble())
                else -> c.setCellValue(value.toString())
            }
        }
    }
}