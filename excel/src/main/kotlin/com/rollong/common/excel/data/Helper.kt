package com.rollong.common.excel.data

fun <T : Any> SheetData.toData(factory: () -> T) = ExcelDataUtil.sheetToData(this, factory)

inline fun <reified T : Any> SheetData.toData(): List<T> {
    val constructor = T::class.java.constructors.firstOrNull {
        it.parameterCount == 0
    } ?: throw RuntimeException("${T::class.java.name}:不存在无参数constructor")
    return this.toData { constructor.newInstance() as T }
}

fun List<Any>.toExcelSheet() = ExcelDataUtil.dataToSheet(this)