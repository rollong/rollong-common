package com.rollong.common.excel.data

@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.PROPERTY_GETTER)
annotation class ExcelDateFormat(
    val pattern: String = "yyyy-MM-dd HH:mm:ss",
    val timezone: String = "GMT+8"
)
