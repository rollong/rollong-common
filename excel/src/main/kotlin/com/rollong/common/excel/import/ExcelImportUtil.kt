package com.rollong.common.excel.import

import com.rollong.common.excel.data.SheetData
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.InputStream
import java.text.DecimalFormat

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 21:19
 * @project rollong-common
 * @filename ExcelImportUtil.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.excel.import
 * @description Excel导入工具类
 */
object ExcelImportUtil {

    private val decimalFormat = DecimalFormat("#.#########")

    fun import(
        excelInputStream: InputStream
    ): List<SheetData> {
        try {
            val wb = XSSFWorkbook(excelInputStream)
            val sheets: MutableList<SheetData> = mutableListOf()
            wb.sheetIterator().forEach { sheet ->
                val sheetData = SheetData(
                    name = sheet.sheetName
                )
                for (i in 0 until sheet.lastRowNum + 1) {
                    //行循环开始
                    val row = sheet.getRow(i)
                    if (null != row) {
                        val rowData = SheetData.Row()
                        //每行的最后一个单元格位置
                        for (j in 0 until row.lastCellNum.toInt()) {
                            //列循环开始
                            val cell = row.getCell(j)
                            val cellValue: Any? = if (null != cell) {
                                when (cell.cellType) {
                                    CellType.NUMERIC -> {
                                        if (DateUtil.isCellDateFormatted(cell)) {
                                            cell.dateCellValue.time
                                        } else {
                                            this.decimalFormat.format(cell.numericCellValue)
                                        }
                                    }
                                    CellType.STRING -> cell.stringCellValue
                                    CellType.FORMULA -> this.decimalFormat.format(cell.numericCellValue)
                                    CellType.BLANK -> null
                                    CellType.BOOLEAN -> cell.booleanCellValue
                                    CellType.ERROR -> cell.errorCellValue.toString()
                                    else -> null
                                }
                            } else {
                                null
                            }
                            rowData.cells.add(
                                SheetData.Cell(
                                    value = cellValue
                                )
                            )
                        }
                        if (0 == i) {
                            sheetData.index = rowData
                        } else {
                            sheetData.rows.add(rowData)
                        }
                    }
                }
                sheets.add(sheetData)
            }
            return sheets
        } catch (e: Exception) {
            throw e
        } finally {
            excelInputStream.close()
        }
    }
}