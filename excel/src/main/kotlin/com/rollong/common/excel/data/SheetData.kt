package com.rollong.common.excel.data

import org.apache.poi.xssf.usermodel.XSSFCellStyle

class SheetData(
    var name: String? = null,
    var index: Row = Row(),
    var rows: MutableList<Row> = mutableListOf()
) {
    constructor(
        mapList: List<Map<String, Any?>>
    ) : this() {
        if (mapList.isNotEmpty()) {
            mapList.forEachIndexed { index, map ->
                if (0 == index) {
                    this.index = Row(map.keys.toList())
                }
                this.rows.add(
                    Row(map.values.toList())
                )
            }
        }
    }

    fun setExplicitList(name: String, explicitList: Array<String>?): SheetData {
        this.index.cells.firstOrNull { it.value == name }?.let {
            it.explicitList = explicitList
        }
        return this
    }

    fun appendRow(values: List<String?>): SheetData {
        this.rows.add(Row(values))
        return this
    }

    fun iterator(): ListIterator<Map<String, Any?>> {
        val mapList: MutableList<Map<String, Any?>> = mutableListOf()
        this.rows.forEach {
            val row = it
            val map: MutableMap<String, Any?> = mutableMapOf()
            this.index.cells.forEachIndexed { index, cell ->
                val key = cell.value as String?
                if (null != key) {
                    map[key] = row.cells.getOrNull(index)?.value
                }
            }
            mapList.add(map)
        }
        return mapList.listIterator()
    }

    class Row(
        var cells: MutableList<Cell> = mutableListOf(),
        var style: XSSFCellStyle? = null
    ) {
        constructor(values: List<Any?>) : this(
            cells = values.map { Cell(value = it) }.toMutableList()
        )
    }

    class Cell(
        var value: Any? = null,
        var style: XSSFCellStyle? = null,
        var width: Int? = null,
        var explicitList: Array<String>? = null
    )
}