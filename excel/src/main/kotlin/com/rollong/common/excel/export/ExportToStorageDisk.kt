package com.rollong.common.excel.export

import com.rollong.common.excel.data.SheetData
import com.rollong.common.storage.StorageDisk
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

fun SheetData.toTemporaryDownlandLink(
    filename: String,
    disk: StorageDisk
): String {
    val outputStream = ByteArrayOutputStream()
    val path = "temp/download/" + SimpleDateFormat("yyyy/MM/dd/").format(Date()) + UUID.randomUUID().toString()
        .replace("-", "") + "/${filename}.xlsx"
    ExcelExportUtil.write(
        sheet = this,
        outputStream = outputStream
    )
    disk.write(
        path = path,
        stream = ByteArrayInputStream(outputStream.toByteArray())
    )
    return disk.publicURL(
        path = path
    )
}