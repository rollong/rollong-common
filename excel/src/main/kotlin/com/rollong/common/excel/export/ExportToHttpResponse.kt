package com.rollong.common.excel.export

import com.rollong.common.excel.data.SheetData
import com.rollong.common.util.encoding.code
import com.rollong.common.util.url.urlencode
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import java.io.ByteArrayOutputStream
import javax.servlet.http.HttpServletResponse

fun SheetData.toResponseEntity(
    filename: String
): ResponseEntity<ByteArray> {
    val headers = HttpHeaders()
    headers.add("Cache-Control", "no-cache, no-store, must-revalidate")
    headers.add("Pragma", "no-cache")
    headers.add("Expires", "0")
    val attachmentName = "${filename}.xlsx".urlencode()
    headers.setContentDispositionFormData("attachment", attachmentName)
    val outputStream = ByteArrayOutputStream()
    ExcelExportUtil.write(
        sheet = this,
        outputStream = outputStream
    )
    return ResponseEntity.ok()
        .headers(headers)
        .contentType(MediaType.parseMediaType("application/vnd.ms-excel;charset=utf-8"))
        .body(outputStream.toByteArray())
}

fun SheetData.toHttpResponse(
    filename: String,
    response: HttpServletResponse,
    urlEncodeFilename: Boolean = true
) {
    try {
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate")
        response.setHeader("Pragma", "no-cache")
        response.setHeader("Expires", "0")
        val attachmentName = if (urlEncodeFilename) {
            "${filename}.xlsx".urlencode()
        } else {
            "${filename}.xlsx".code(Charsets.ISO_8859_1)
        }
        response.setHeader("Content-Disposition", "attachment;filename=${attachmentName}")
        response.contentType = "application/octet-stream"
        ExcelExportUtil.write(
            sheet = this,
            outputStream = response.outputStream
        )
    } catch (e: Exception) {
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
        throw e
    }
}