#!/bin/bash

./gradlew :clean \
:dto:clean dto:build \
:excel:clean excel:build \
:exception:clean exception:build \
:ioc-spring:clean ioc-spring:build \
:json:clean json:build \
:mail:clean mail:build \
:net:clean net:build \
:qrcode:clean qrcode:build \
:response:clean response:build \
:snowflake:clean snowflake:build \
:sql:clean sql:build \
:storage:clean storage:build \
:storage-aliyun:clean storage-aliyun:build \
:time:clean time:build \
:util:clean util:build \
:zip:clean zip:build
