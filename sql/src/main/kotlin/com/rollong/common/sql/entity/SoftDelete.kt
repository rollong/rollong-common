package com.rollong.common.sql.entity

import java.io.Serializable
import java.sql.Timestamp

interface SoftDelete<U : Serializable> {
    var deletedAt: Timestamp?
    var deletedBy: U?
}