package com.rollong.common.sql.mybatis.handler

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedJdbcTypes
import org.apache.ibatis.type.MappedTypes
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.*

@MappedTypes(Long::class)
@MappedJdbcTypes(JdbcType.TIMESTAMP)
class UUIDTypeHandler : BaseTypeHandler<UUID?>() {


    override fun getNullableResult(cs: CallableStatement?, columnIndex: Int): UUID? {
        return cs?.getString(columnIndex)?.let { UUID.fromString(it) }
    }

    //获取数据结果集时把数据库类型转换为对应的Java类型
    override fun getNullableResult(rs: ResultSet?, columnIndex: Int): UUID? {
        return rs?.getString(columnIndex)?.let { UUID.fromString(it) }
    }

    override fun getNullableResult(rs: ResultSet?, columnName: String?): UUID? {
        return rs?.getString(columnName)?.let { UUID.fromString(it) }
    }

    override fun setNonNullParameter(ps: PreparedStatement?, i: Int, parameter: UUID?, jdbcType: JdbcType?) {
        ps!!.setString(i, parameter?.toString())
    }
}