package com.rollong.common.sql.entity

import java.io.Serializable
import java.sql.Timestamp

interface HasTimestamps<U : Serializable> {
    var createdAt: Timestamp
    var createdBy: U?
    var updatedAt: Timestamp
    var updatedBy: U?
}