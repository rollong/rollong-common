package com.rollong.common.sql.hibernate.usertype

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-12 16:56
 * @project rollong-common
 * @filename JSONStringArrayUserType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.hibernate.usertype
 * @description
 */
class JSONStringArrayUserType : JSONArrayUserType<String>(String::class.java) {
}