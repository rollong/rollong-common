package com.rollong.common.sql.hibernate.usertype

import com.rollong.common.json.JSONUtil
import org.hibernate.engine.spi.SharedSessionContractImplementor
import java.sql.ResultSet


/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/27 17:59
 * @project 诺朗java-common库
 * @filename JSONArrayUserType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.hibernate.usertype
 * @description  返回JSONArray
 */
open class JSONArrayUserType<T : Any>(private val clazz: Class<T>) : JSONUserType() {

    override fun returnedClass() = List::class.java

    override fun nullSafeGet(
        rs: ResultSet,
        names: Array<String>,
        session: SharedSessionContractImplementor,
        owner: Any?
    ): Any? {
        return if (rs.getString(names[0]) == null) {
            null
        } else {
            JSONUtil.parseArray(rs.getString(names[0]), this.clazz)
        }
    }
}