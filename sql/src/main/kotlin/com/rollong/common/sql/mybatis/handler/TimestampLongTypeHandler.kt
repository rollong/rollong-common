package com.rollong.common.sql.mybatis.handler

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedJdbcTypes
import org.apache.ibatis.type.MappedTypes
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Timestamp

@MappedTypes(Long::class)
@MappedJdbcTypes(JdbcType.TIMESTAMP)
class TimestampLongTypeHandler : BaseTypeHandler<Long?>() {


    override fun getNullableResult(cs: CallableStatement?, columnIndex: Int): Long? {
        return cs?.getTimestamp(columnIndex)?.time
    }

    //获取数据结果集时把数据库类型转换为对应的Java类型
    override fun getNullableResult(rs: ResultSet?, columnIndex: Int): Long? {
        return rs?.getTimestamp(columnIndex)?.time
    }

    override fun getNullableResult(rs: ResultSet?, columnName: String?): Long? {
        return rs?.getTimestamp(columnName)?.time
    }

    override fun setNonNullParameter(ps: PreparedStatement?, i: Int, parameter: Long?, jdbcType: JdbcType?) {
        if (null != parameter) {
            ps!!.setTimestamp(i, Timestamp(parameter))
        } else {
            ps!!.setTimestamp(i, null)
        }
    }
}