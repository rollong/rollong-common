package com.rollong.common.sql.entity

import java.io.Serializable

abstract class AbstractEntity<PK : Serializable> : Serializable {
    abstract var id: PK?

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AbstractEntity<*>) return false
        if (this.id == null || other.id == null) return false
        if (id != other.id) return false
        if (this.javaClass != other.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: super.hashCode()
    }
}