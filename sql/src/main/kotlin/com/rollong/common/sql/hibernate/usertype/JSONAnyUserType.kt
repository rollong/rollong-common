package com.rollong.common.sql.hibernate.usertype

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/27 18:00
 * @project 诺朗java-common库
 * @filename JSONAnyUserType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.hibernate.usertype
 * @description  json字段返回Any
 */
class JSONAnyUserType : JSONUserType() {
    override fun returnedClass() = Any::class.java
}