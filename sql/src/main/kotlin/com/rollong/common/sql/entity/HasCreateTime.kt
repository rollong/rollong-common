package com.rollong.common.sql.entity

import java.sql.Timestamp

interface HasCreateTime {
    var createdAt: Timestamp
}