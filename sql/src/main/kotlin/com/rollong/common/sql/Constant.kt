package com.rollong.common.sql

import java.sql.Timestamp

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-18 14:31
 * @project rollong-common
 * @filename Constant.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql
 * @description
 */
object Constant {
    val genesis = Timestamp(0)
}