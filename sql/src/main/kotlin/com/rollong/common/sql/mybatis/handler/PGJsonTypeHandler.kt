package com.rollong.common.sql.mybatis.handler

import com.rollong.common.json.JSONUtil
import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import org.postgresql.util.PGobject
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException


/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/02 21:21
 * @project rollong-common
 * @filename PGJsonTypeHandler.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.mybatis.handler
 * @description  处理postgres json类型字段
 */
@MappedTypes(Any::class)
open class PGJsonTypeHandler(
    private val clazz: Class<*>
) : BaseTypeHandler<Any?>() {

    @Throws(SQLException::class)
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: Any?, jdbcType: JdbcType?) {
        if (null != parameter) {
            val jsonObject = PGobject()
            jsonObject.type = "json"
            jsonObject.value = JSONUtil.toJSONString(parameter)
            ps.setObject(i, jsonObject)
        } else {
            ps.setObject(i, null)
        }
    }

    private fun parseObject(json: String?): Any? {
        return if (null == json) {
            null
        } else {
            JSONUtil.parseObject(json, this.clazz)
        }
    }

    @Throws(SQLException::class)
    override fun getNullableResult(rs: ResultSet?, columnName: String): Any? {
        val json = rs?.getString(columnName)
        return this.parseObject(json)
    }

    @Throws(SQLException::class)
    override fun getNullableResult(rs: ResultSet?, columnIndex: Int): Any? {
        val json = rs?.getString(columnIndex)
        return this.parseObject(json)
    }

    @Throws(SQLException::class)
    override fun getNullableResult(cs: CallableStatement?, columnIndex: Int): Any? {
        val json = cs?.getString(columnIndex)
        return this.parseObject(json)
    }
}