package com.rollong.common.sql.entity

import java.io.Serializable
import java.sql.Timestamp

fun <U : Serializable> HasTimestamps<U>.created(
    by: U? = null
) {
    val now = Timestamp(System.currentTimeMillis())
    this.createdAt = now
    this.createdBy = by
    this.updatedAt = now
    this.updatedBy = by
}

fun <U : Serializable> HasTimestamps<U>.updated(
    by: U? = null
) {
    val now = Timestamp(System.currentTimeMillis())
    this.updatedAt = now
    this.updatedBy = by
}

fun <U : Serializable> SoftDelete<U>.deleted(
    by: U? = null
) {
    val now = Timestamp(System.currentTimeMillis())
    this.deletedAt = now
    this.deletedBy = by
}

@Suppress("UNUSED_PARAMETER")
fun <U : Serializable> SoftDelete<U>.restored(
    by: U? = null
) {
    this.deletedAt = null
    this.deletedBy = null
}

fun <U : Serializable> SoftDelete<U>.inTrash() = this.deletedAt != null

fun <U : Serializable> SoftDelete<U>.notInTrash() = this.deletedAt == null

fun HasCreateTime.created() {
    val now = Timestamp(System.currentTimeMillis())
    this.createdAt = now
    if (this is HasUpdateTime) {
        this.updatedAt = now
    }
}

fun HasUpdateTime.updated() {
    this.updatedAt = Timestamp(System.currentTimeMillis())
}