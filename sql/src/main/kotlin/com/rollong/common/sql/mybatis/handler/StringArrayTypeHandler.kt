package com.rollong.common.sql.mybatis.handler

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * @link http://blog.csdn.net/libraryhu/article/details/71141793
 */
class StringArrayTypeHandler : BaseTypeHandler<Array<String>>() {

    @Throws(SQLException::class)
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: Array<String>, jdbcType: JdbcType) {
        val typeName = TYPE_NAME_VARCHAR
        // 这3行是关键的代码，创建Array，然后ps.setArray(i, array)就可以了
        val conn = ps.connection
        val array = conn.createArrayOf(typeName, parameter)
        ps.setArray(i, array)

    }

    @Suppress("UNCHECKED_CAST")
    @Throws(SQLException::class)
    override fun getNullableResult(resultSet: ResultSet, s: String): Array<String>? {
        return resultSet.getArray(s)?.array as Array<String>?
    }

    @Suppress("UNCHECKED_CAST")
    @Throws(SQLException::class)
    override fun getNullableResult(resultSet: ResultSet, i: Int): Array<String>? {
        return resultSet.getArray(i)?.array as Array<String>?
    }

    @Suppress("UNCHECKED_CAST")
    @Throws(SQLException::class)
    override fun getNullableResult(callableStatement: CallableStatement, i: Int): Array<String>? {
        return callableStatement.getArray(i).array as Array<String>?
    }

    companion object {
        private val TYPE_NAME_VARCHAR = "varchar"
        private val TYPE_NAME_INTEGER = "integer"
        private val TYPE_NAME_BOOLEAN = "boolean"
        private val TYPE_NAME_NUMERIC = "numeric"
    }
}
