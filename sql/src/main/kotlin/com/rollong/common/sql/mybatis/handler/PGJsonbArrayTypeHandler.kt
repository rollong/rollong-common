package com.rollong.common.sql.mybatis.handler

import com.rollong.common.json.JSONUtil
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import org.postgresql.util.PGobject
import java.sql.PreparedStatement
import java.sql.SQLException

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 01:01
 * @project rollong-common
 * @filename PGJsonbArrayTypeHandler.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.mybatis.handler
 * @description
 */
@MappedTypes(Any::class)
class PGJsonbArrayTypeHandler(
    clazz: Class<*>
) : PGJsonArrayTypeHandler(
    clazz
) {
    @Throws(SQLException::class)
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: Any?, jdbcType: JdbcType?) {
        if (null != parameter) {
            val jsonObject = PGobject()
            jsonObject.type = "jsonb"
            jsonObject.value = JSONUtil.toJSONString(parameter)
            ps.setObject(i, jsonObject)
        } else {
            ps.setObject(i, null)
        }
    }
}