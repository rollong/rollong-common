package com.rollong.common.sql.mybatis

import com.baomidou.mybatisplus.annotation.FieldFill
import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import com.rollong.common.sql.Constant
import com.rollong.common.sql.entity.AbstractEntity
import com.rollong.common.sql.entity.HasCreateTime
import com.rollong.common.sql.entity.HasUpdateTime
import java.io.Serializable
import java.sql.Timestamp
import javax.persistence.MappedSuperclass

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-18 14:27
 * @project rollong-common
 * @filename MybatisBaseEntityWithTimestamps.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.entity
 * @description
 */
@MappedSuperclass
abstract class MybatisBaseEntityWithTimestamps<T : Serializable> : AbstractEntity<T>(), HasCreateTime, HasUpdateTime {
    @TableId
    override var id: T? = null

    @TableField(fill = FieldFill.INSERT)
    open override var createdAt: Timestamp = Constant.genesis

    @TableField(fill = FieldFill.INSERT_UPDATE)
    open override var updatedAt: Timestamp = Constant.genesis
}