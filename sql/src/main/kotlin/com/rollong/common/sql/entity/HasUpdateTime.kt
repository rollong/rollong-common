package com.rollong.common.sql.entity

import java.sql.Timestamp

interface HasUpdateTime {
    var updatedAt: Timestamp
}