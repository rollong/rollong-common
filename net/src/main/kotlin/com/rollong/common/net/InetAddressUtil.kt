package com.rollong.common.net

import com.google.common.net.InetAddresses
import com.rollong.common.util.collection.repeats
import java.net.Inet4Address
import java.net.InetAddress
import kotlin.experimental.and

object InetAddressUtil {

    class CIDR(
        val address: ByteArray,
        val maskLength: Int
    ) {
        companion object {
            fun fromString(cidr: String) = cidr.split('/').let {
                CIDR(
                    address = InetAddressUtil.fromString(it[0])?.address
                        ?: throw IllegalArgumentException("wrong IP address string:${it[0]}"),
                    maskLength = it[1].toInt()
                )
            }
        }

        fun inRange(otherAddress: ByteArray): Boolean {
            return sameSubnet(
                address = this.address,
                otherAddress = otherAddress,
                maskLength = this.maskLength
            )
        }

        fun inRange(ipString: String) = InetAddressUtil.fromString(ipString)?.let { this.inRange(it.address) } ?: false
    }

    fun sameSubnet(
        address: ByteArray,
        otherAddress: ByteArray,
        maskLength: Int
    ): Boolean {
        if (address.size != otherAddress.size) {
            return false
        }
        if (address.size == 4) {
            val ipAddr = this.ipv4toLong(address)
            val otherIpAddr = this.ipv4toLong(otherAddress)
            val mask = -0x1L shl 32 - maskLength
            return ipAddr.and(mask) == otherIpAddr.and(mask)
        }
        if (address.size == 6) {
            val maskArray = this.mask(length = maskLength, size = address.size)
            address.forEachIndexed { index, addrByte ->
                val otherAddrByte = otherAddress[index]
                val maskByte = maskArray[index]
                if (addrByte.and(maskByte) != otherAddrByte.and(maskByte)) {
                    return false
                }
            }
            return true
        }
        return false
    }

    fun mask(length: Int, size: Int): ByteArray {
        val arr = ByteArray(size)
        size.repeats {
            when {
                length > (it + 1) * 8 -> arr[it] = -0x1
                length < it * 8 -> arr[it] = 0
                else -> arr[it] = (-0x1 shl (8 - length % 8)).toByte()
            }
        }
        return arr
    }

    fun ipv4toLong(address: Inet4Address) = this.ipv4toLong(address = address.address)

    //ipv4 32bit int4 ipv6 128bit int16
    fun ipv4toLong(address: ByteArray): Long {
        if (address.size != 4) {
            throw IllegalArgumentException("invalid ipv4 address")
        }
        var result = 0L
        address.forEach {
            result = (result.shl(8) + it.toLong())
        }
        return result
    }

    /**
     * 把xx.xx.xx.xx类型的转为long类型的
     *
     * @param ip
     * @return
     */
    fun ipv4toLong(ip: String): Long {
        var result = 0L
        for (s in ip.split('.').dropLastWhile { it.isEmpty() }.toTypedArray()) {
            result = result.shl(8) + s.toInt()
        }
        return result
    }

    /**
     * 把long类型的Ip转为一般Ip类型：xx.xx.xx.xx
     *
     * @param ip
     * @return
     */
    fun long2ipv4(ip: Long): String {
        val s1 = (ip and 4278190080L).shr(24)
        val s2 = (ip and 16711680L).shr(16)
        val s3 = (ip and 65280L).shr(8)
        val s4 = ip and 255L
        return "$s1.$s2.$s3.$s4"
    }

    fun fromString(ipString: String): InetAddress? {
        return try {
            InetAddresses.forString(ipString)
        } catch (e: IllegalArgumentException) {
            null
        }
    }
}