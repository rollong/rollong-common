package com.rollong.common.net

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import java.util.regex.Pattern

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/10/28 16:19
 * @project 诺朗java-common库
 * @filename IPv4Util.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.inet
 * @description
 */
object IPv4Util {

    /**
     * 功能：判断一个IP是不是在一个网段下的
     * 格式：isInRange("192.168.8.3", "192.168.9.10/22");
     */
    fun isInRange(ip: String, cidr: String): Boolean {
        val ips = ip.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val ipAddr = (Integer.parseInt(ips[0]) shl 24
            or (Integer.parseInt(ips[1]) shl 16)
            or (Integer.parseInt(ips[2]) shl 8) or Integer.parseInt(ips[3]))
        val type = Integer.parseInt(cidr.replace(".*/".toRegex(), ""))
        val mask = -0x1 shl 32 - type
        val cidrIp = cidr.replace("/.*".toRegex(), "")
        val cidrIps = cidrIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val cidrIpAddr = (Integer.parseInt(cidrIps[0]) shl 24
            or (Integer.parseInt(cidrIps[1]) shl 16)
            or (Integer.parseInt(cidrIps[2]) shl 8)
            or Integer.parseInt(cidrIps[3]))
        return ipAddr and mask == cidrIpAddr and mask
    }

    /**
     * 功能：根据IP和位数返回该IP网段的所有IP
     * 格式：parseIpMaskRange("192.192.192.1.", 23)
     */
    fun parseIpMaskRange(ip: String, mask: Int): List<String> {
        var list: MutableList<String> = ArrayList()
        if (32 == mask) {
            list.add(ip)
        } else {
            var startIp = getBeginIpStr(ip, mask)
            var endIp = getEndIpStr(ip, mask)
            if (31 != mask) {
                val subStart = startIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[0] + "." + startIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[1] + "." + startIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[2] + "."
                val subEnd = endIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[0] + "." + endIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[1] + "." + endIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[2] + "."
                startIp = subStart + (Integer.parseInt(startIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[3]) + 1)
                endIp = subEnd + (Integer.parseInt(endIp.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()[3]) - 1)
            }
            list = parseIpRange(startIp, endIp)
        }
        return list
    }

    /**
     * 功能：根据位数返回IP总数
     * 格式：parseIpMaskRange("192.192.192.1", "23")
     */
    @SuppressWarnings
    fun getIpCount(mask: String): Int {
        return BigDecimal.valueOf(Math.pow(2.0, (32 - Integer.parseInt(mask)).toDouble()))
            .setScale(0, RoundingMode.DOWN).toInt()//IP总数，去小数点
    }

    /**
     * 功能：根据位数返回IP总数
     * 格式：isIP("192.192.192.1")
     */
    fun isIP(str: String): Boolean {
        val regex = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"
        val pattern = Pattern.compile(regex)
        return pattern.matcher(str).matches()
    }

    fun parseIpRange(ipfrom: String, ipto: String): MutableList<String> {
        val ips = ArrayList<String>()
        val ipfromd = ipfrom.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val iptod = ipto.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val int_ipf = IntArray(4)
        val int_ipt = IntArray(4)
        for (i in 0..3) {
            int_ipf[i] = Integer.parseInt(ipfromd[i])
            int_ipt[i] = Integer.parseInt(iptod[i])
        }
        for (A in int_ipf[0]..int_ipt[0]) {
            for (B in (if (A == int_ipf[0]) int_ipf[1] else 0)..if (A == int_ipt[0]) int_ipt[1] else 255) {
                for (C in (if (B == int_ipf[1]) int_ipf[2] else 0)..if (B == int_ipt[1]) int_ipt[2] else 255) {
                    for (D in (if (C == int_ipf[2]) int_ipf[3] else 0)..if (C == int_ipt[2]) int_ipt[3] else 255) {
                        ips.add("$A.$B.$C.$D")
                    }
                }
            }
        }
        return ips
    }

    /**
     * 根据掩码位获取掩码
     *
     * @param maskBit
     * 掩码位数，如"28"、"30"
     * @return
     */
    fun getMaskByMaskBit(maskBit: Int): String {
        return this.getMaskMap(maskBit)
    }

    /**
     * 根据 ip/掩码位 计算IP段的起始IP 如 IP串 218.240.38.69/30
     *
     * @param ip
     * 给定的IP，如218.240.38.69
     * @param maskBit
     * 给定的掩码位，如30
     * @return 起始IP的字符串表示
     */
    fun getBeginIpStr(ip: String, maskBit: Int): String {
        return long2ip(getBeginIpLong(ip, maskBit))
    }

    /**
     * 根据 ip/掩码位 计算IP段的起始IP 如 IP串 218.240.38.69/30
     *
     * @param ip
     * 给定的IP，如218.240.38.69
     * @param maskBit
     * 给定的掩码位，如30
     * @return 起始IP的长整型表示
     */
    fun getBeginIpLong(ip: String, maskBit: Int): Long {
        return this.ip2long(ip) and this.ip2long(this.getMaskByMaskBit(maskBit))
    }

    /**
     * 根据 ip/掩码位 计算IP段的终止IP 如 IP串 218.240.38.69/30
     *
     * @param ip
     * 给定的IP，如218.240.38.69
     * @param maskBit
     * 给定的掩码位，如30
     * @return 终止IP的字符串表示
     */
    fun getEndIpStr(ip: String, maskBit: Int): String {
        return this.long2ip(this.getEndIpLong(ip, maskBit))
    }

    /**
     * 根据 ip/掩码位 计算IP段的终止IP 如 IP串 218.240.38.69/30
     *
     * @param ip
     * 给定的IP，如218.240.38.69
     * @param maskBit
     * 给定的掩码位，如30
     * @return 终止IP的长整型表示
     */
    fun getEndIpLong(ip: String, maskBit: Int): Long {
        return this.getBeginIpLong(ip, maskBit) + this.ip2long(this.getMaskByMaskBit(maskBit)).inv()
    }


    /**
     * 根据子网掩码转换为掩码位 如 255.255.255.252转换为掩码位 为 30
     *
     * @param netmarks
     * @return
     */
    fun getNetMask(netmarks: String): Int {
        var sbf: StringBuilder
        var str: String
        var inetmask = 0
        val ipList = netmarks.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (n in ipList.indices) {
            sbf = toBin(Integer.parseInt(ipList[n]))
            str = sbf.reverse().toString()
            var count = 0
            var i = 0
            while (i < str.length) {
                i = str.indexOf('1', i)
                if (i == -1) {
                    break
                }
                count++
                i++
            }
            inetmask += count
        }
        return inetmask
    }

    /**
     * 计算子网大小
     *
     * @param netmask
     * 掩码位
     * @return
     */
    fun getPoolMax(maskBit: Int): Int {
        return if (maskBit <= 0 || maskBit >= 32) {
            0
        } else Math.pow(2.0, (32 - maskBit).toDouble()).toInt() - 2
    }

    private fun toBin(x: Int): StringBuilder {
        var xvar = x
        val result = StringBuilder()
        result.append(xvar % 2)
        xvar /= 2
        while (x > 0) {
            result.append(xvar % 2)
            xvar /= 2
        }
        return result
    }

    fun getMaskMap(maskBit: Int): String {
        return when (maskBit) {
            1 -> "128.0.0.0"
            2 -> "192.0.0.0"
            3 -> "224.0.0.0"
            4 -> "240.0.0.0"
            5 -> "248.0.0.0"
            6 -> "252.0.0.0"
            7 -> "254.0.0.0"
            8 -> "255.0.0.0"
            9 -> "255.128.0.0"
            10 -> "255.192.0.0"
            11 -> "255.224.0.0"
            12 -> "255.240.0.0"
            13 -> "255.248.0.0"
            14 -> "255.252.0.0"
            15 -> "255.254.0.0"
            16 -> "255.255.0.0"
            17 -> "255.255.128.0"
            18 -> "255.255.192.0"
            19 -> "255.255.224.0"
            20 -> "255.255.240.0"
            21 -> "255.255.248.0"
            22 -> "255.255.252.0"
            23 -> "255.255.254.0"
            24 -> "255.255.255.0"
            25 -> "255.255.255.128"
            26 -> "255.255.255.192"
            27 -> "255.255.255.224"
            28 -> "255.255.255.240"
            29 -> "255.255.255.248"
            30 -> "255.255.255.252"
            31 -> "255.255.255.254"
            32 -> "255.255.255.255"
            else -> throw RuntimeException("wrong maskBit:${maskBit}")
        }
    }

    /**
     * 把xx.xx.xx.xx类型的转为long类型的
     *
     * @param ip
     * @return
     */
    fun ip2long(ip: String): Long {
        var result = 0L
        for (s in ip.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
            result = ((result) shl 8) + s.toInt()
        }
        return result
    }

    /**
     * 把long类型的Ip转为一般Ip类型：xx.xx.xx.xx
     *
     * @param ip
     * @return
     */
    fun long2ip(ip: Long): String {
        val s1 = ((ip and 4278190080L) / 16777216L).toString()
        val s2 = ((ip and 16711680L) / 65536L).toString()
        val s3 = ((ip and 65280L) / 256L).toString()
        val s4 = (ip and 255L).toString()
        return "$s1.$s2.$s3.$s4"
    }
}