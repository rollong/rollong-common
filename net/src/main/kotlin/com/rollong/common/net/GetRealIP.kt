package com.rollong.common.net

import java.net.InetAddress
import javax.servlet.http.HttpServletRequest

fun HttpServletRequest.getRealIp(
    proxies: List<InetAddressUtil.CIDR> = listOf(
        InetAddressUtil.CIDR.fromString("127.0.0.1/32"),
        InetAddressUtil.CIDR.fromString("10.0.0.0/8"),
        InetAddressUtil.CIDR.fromString("172.16.0.0/12"),
        InetAddressUtil.CIDR.fromString("192.168.0.0/16")
    )
): String {
    //代理进来，则透过防火墙获取真实IP地址
    var ip: InetAddress? = null
    listOf(
        "X-Forwarded-For",
        "Proxy-Client-IP",
        "WL-Proxy-Client-IP",
        "HTTP_CLIENT_IP",
        "HTTP_X_FORWARDED_FOR",
        "X-Real-IP"
    ).forEach {
        val header = this.getHeader(it)?.toLowerCase()?.trim()
        if (!header.isNullOrBlank() && "unknown" != header) {
            header.split(",").forEach {
                val ipFromHeader = it.trim()
                var isProxy = false
                ip = InetAddressUtil.fromString(ipFromHeader)
                ip?.let {
                    for (proxy in proxies) {
                        if (proxy.inRange(it.address)) {
                            isProxy = true
                            break
                        }
                    }
                    if (!isProxy) {
                        return it.hostAddress ?: ipFromHeader
                    }
                }
            }
        }
    }
    val ipString = ip?.hostAddress ?: this.remoteAddr
    return if (ipString == "0:0:0:0:0:0:0:1") "127.0.0.1" else ipString ?: "unknown"
}