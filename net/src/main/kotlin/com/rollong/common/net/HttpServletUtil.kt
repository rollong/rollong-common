package com.rollong.common.net

import javax.servlet.http.HttpServletRequest

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/10/28 16:20
 * @project 诺朗java-common库
 * @filename HttpServletUtil.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.inet
 * @description
 */

/**
 * @author kwang on 2019/8/25 20:32
 * @param proxies 局域网转发代理
 * @return 客户端真实ip地址
 * @description 通过http header获取客户端ip地址
 */
fun HttpServletRequest.getRealIp(
    proxies: List<String> = listOf("127.0.0.1/32", "172.16.0.0/12", "192.168.0.0/16"),
    proxies6: List<String> = listOf()
): String {
    //代理进来，则透过防火墙获取真实IP地址
    var ip: String? = null
    listOf(
        "X-Forwarded-For",
        "Proxy-Client-IP",
        "WL-Proxy-Client-IP",
        "HTTP_CLIENT_IP",
        "HTTP_X_FORWARDED_FOR",
        "X-Real-IP"
    ).forEach {
        val header = this.getHeader(it)?.toLowerCase()?.trim()
        if (!header.isNullOrBlank() && "unknown" != header) {
            header.split(",").forEach {
                val ipFromHeader = it.trim()
                var isProxy = false
                ip = ipFromHeader
                if (IPv4Util.isIP(ipFromHeader)) {
                    for (proxy in proxies) {
                        if (IPv4Util.isInRange(ip = ipFromHeader, cidr = proxy)) {
                            isProxy = true
                        }
                    }
                    if (!isProxy) {
                        return ipFromHeader
                    }
                }
                if (IPv6Util.isValidIPv6(ipFromHeader)) {
                    for (proxy in proxies6) {
                        if (IPv6Util.isInRange(ip = ipFromHeader, cidr = proxy)) {
                            isProxy = true
                        }
                    }
                    if (!isProxy) {
                        return ipFromHeader
                    }
                }
            }
        }
    }
    ip = ip ?: this.remoteAddr
    return if (ip == "0:0:0:0:0:0:0:1") "127.0.0.1" else ip ?: "unknown"
}