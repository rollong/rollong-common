package com.rollong.common.qrcode

import com.google.zxing.BinaryBitmap
import com.google.zxing.DecodeHintType
import com.google.zxing.MultiFormatReader
import com.google.zxing.client.j2se.BufferedImageLuminanceSource
import com.google.zxing.common.HybridBinarizer
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import javax.imageio.ImageIO

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 10:29
 * @project rollong-common
 * @filename QRCodeDecoder.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.qrcode
 * @description 二维码解码
 */
object QRCodeDecoder {

    @Throws(IOException::class)
    fun decode(inputStream: InputStream, charset: String = "utf-8"): String? {
        val image = ImageIO.read(inputStream) ?: return null
        return this.decode(image, charset)
    }

    fun decode(byteArray: ByteArray, charset: String = "utf-8"): String? {
        val image = ImageIO.read(ByteArrayInputStream(byteArray)) ?: return null
        return this.decode(image, charset)
    }

    private fun decode(image: BufferedImage, charset: String): String? {
        val source = BufferedImageLuminanceSource(image)
        val bitmap = BinaryBitmap(HybridBinarizer(source))
        val hints: MutableMap<DecodeHintType, Any> = mutableMapOf()
        hints[DecodeHintType.CHARACTER_SET] = charset
        val result = MultiFormatReader().decode(bitmap, hints)
        return result.text
    }

}