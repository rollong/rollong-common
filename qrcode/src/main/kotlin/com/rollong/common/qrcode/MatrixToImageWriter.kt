package com.rollong.common.qrcode

import com.google.zxing.common.BitMatrix
import java.awt.image.BufferedImage
import java.io.File
import java.io.IOException
import java.io.OutputStream
import javax.imageio.ImageIO

/**
 * Copyright 成都诺朗科技有限公司
 * @author [binarywang(Binary Wang)](https://github.com/binarywang)
 * @createdAt 2020-12-26 12:44
 * @project rollong-common
 * @filename MatrixToImageWriter.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.qrcode
 * @description Created by Binary Wang on 2017-01-05.
 */
object MatrixToImageWriter {
    private const val BLACK = -0x1000000
    private const val WHITE = -0x1
    private fun toBufferedImage(matrix: BitMatrix): BufferedImage {
        val width = matrix.width
        val height = matrix.height
        val image = BufferedImage(
            width, height,
            BufferedImage.TYPE_INT_RGB
        )
        for (x in 0 until width) {
            for (y in 0 until height) {
                image.setRGB(x, y, if (matrix[x, y]) BLACK else WHITE)
            }
        }
        return image
    }

    @Throws(IOException::class)
    fun writeToFile(matrix: BitMatrix, format: String, file: File) {
        val image = this.toBufferedImage(matrix)
        if (!ImageIO.write(image, format, file)) {
            throw IOException("Could not write an image of format $format to $file")
        }
    }

    @Throws(IOException::class)
    fun writeToStream(matrix: BitMatrix, format: String, stream: OutputStream?) {
        val image = this.toBufferedImage(matrix)
        if (!ImageIO.write(image, format, stream)) {
            throw IOException("Could not write an image of format $format")
        }
    }
}