package com.rollong.common.qrcode

import com.google.zxing.LuminanceSource
import com.google.zxing.client.j2se.BufferedImageLuminanceSource
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage

/**
 * Copyright 成都诺朗科技有限公司
 * @author [binarywang(Binary Wang)](https://github.com/binarywang)
 * @createdAt 2020-12-26 12:46
 * @project rollong-common
 * @filename BufferedImageLuminanceSource.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.qrcode
 * @description
 */
class BufferedImageLuminanceSource @JvmOverloads constructor(
    image: BufferedImage, left: Int = 0,
    top: Int = 0, width: Int = image.width, height: Int = image.height
) :
    LuminanceSource(width, height) {
    private val image: BufferedImage
    private val left: Int
    private val top: Int
    override fun getMatrix(): ByteArray {
        val width = width
        val height = height
        val area = width * height
        val matrix = ByteArray(area)
        image.raster.getDataElements(left, top, width, height, matrix)
        return matrix
    }

    override fun getRow(y: Int, row: ByteArray?): ByteArray {
        require(!(y < 0 || y >= height)) { "Requested row is outside the image: $y" }
        val width = width
        val r = if (row == null || row.size < width) {
            ByteArray(width)
        } else {
            row
        }
        image.raster.getDataElements(left, top + y, width, 1, r)
        return r
    }

    override fun isCropSupported(): Boolean {
        return true
    }

    override fun crop(left: Int, top: Int, width: Int, height: Int): LuminanceSource {
        return BufferedImageLuminanceSource(image, this.left + left, this.top + top, width, height)
    }

    override fun isRotateSupported(): Boolean {
        return true
    }

    override fun rotateCounterClockwise(): LuminanceSource {
        val sourceWidth = image.width
        val sourceHeight = image.height
        val transform = AffineTransform(0.0, -1.0, 1.0, 0.0, 0.0, sourceWidth.toDouble())
        val rotatedImage = BufferedImage(sourceHeight, sourceWidth, BufferedImage.TYPE_BYTE_GRAY)
        val g = rotatedImage.createGraphics()
        g.drawImage(image, transform, null)
        g.dispose()
        val width = width
        return BufferedImageLuminanceSource(rotatedImage, top, sourceWidth - (left + width), height, width)
    }

    init {
        val sourceWidth = image.width
        val sourceHeight = image.height
        require(!(left + width > sourceWidth || top + height > sourceHeight)) { "Crop rectangle does not fit within image data." }
        for (y in top until top + height) {
            for (x in left until left + width) {
                if (image.getRGB(x, y) and -0x1000000 == 0) {
                    image.setRGB(x, y, -0x1) // = white
                }
            }
        }
        this.image = BufferedImage(sourceWidth, sourceHeight, BufferedImage.TYPE_BYTE_GRAY)
        this.image.graphics.drawImage(image, 0, 0, null)
        this.left = left
        this.top = top
    }
}