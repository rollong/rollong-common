package com.rollong.common.qrcode

import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.client.j2se.MatrixToImageWriter
import java.io.OutputStream

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 9:42
 * @project rollong-common
 * @filename QRCodeUtil.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.qrcode
 * @description
 */
object QRCodeUtil {

    fun encode(
        content: String,
        width: Int = 400,
        height: Int = 400,
        format: String = "png",
        outputStream: OutputStream
    ) {
        val hints = mutableMapOf<EncodeHintType, Any>()
        hints[EncodeHintType.CHARACTER_SET] = "UTF-8"
        val bitMatrix = MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints)// 生成矩阵
        MatrixToImageWriter.writeToStream(bitMatrix, format, outputStream)// 输出图像
    }
}