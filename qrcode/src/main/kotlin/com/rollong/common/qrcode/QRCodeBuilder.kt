package com.rollong.common.qrcode

import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import java.awt.*
import java.awt.geom.RoundRectangle2D
import java.awt.image.BufferedImage
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import javax.imageio.ImageIO

/**
 * Copyright 成都诺朗科技有限公司
 * @author [binarywang(Binary Wang)](https://github.com/binarywang)
 * @createdAt 2020-12-26 10:36
 * @project rollong-common
 * @filename QRCodeEncoder.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.qrcode
 * @description 生成二维码
 *
 * val text = "http://www.cnblogs.com/klslb/" //二维码的链接地址
 * //String path = new QRCodeUtil().getClass().getClassLoader().getResource("image/qrLogo.jpg").getPath();
 * this.encode(text, "d:/QRCodeDoc/qrLogo.jpg", "d:/QRCodeDoc", true)
 */
class QRCodeBuilder(
    val qrSize: Int = 300,
    content: String,
    val charset: String = "utf-8"
) {

    private var qrImage: BufferedImage

    init {
        val hints: MutableMap<EncodeHintType, Any> = mutableMapOf()
        hints[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.H
        hints[EncodeHintType.CHARACTER_SET] = charset
        hints[EncodeHintType.MARGIN] = 1
        val bitMatrix = MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, qrSize, qrSize, hints)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val image = BufferedImage(
            width, height,
            BufferedImage.TYPE_INT_RGB
        )
        for (x in 0 until width) {
            for (y in 0 until height) {
                image.setRGB(x, y, if (bitMatrix[x, y]) -0x1000000 else -0x1)
            }
        }
        this.qrImage = image
    }

    fun logo(
        logo: BufferedImage,
        w: Int = 60,
        h: Int = 60,
        compress: Boolean = true
    ): QRCodeBuilder {
        var width = logo.getWidth(null)
        var height = logo.getHeight(null)
        val logoImage: Image = if (compress) {
            if (width > w) {
                width = w
            }
            if (height > h) {
                height = h
            }
            val image = logo.getScaledInstance(
                width, height,
                Image.SCALE_SMOOTH
            )
            val tag = BufferedImage(
                width, height,
                BufferedImage.TYPE_INT_RGB
            )
            val g = tag.graphics
            g.drawImage(image, 0, 0, null) // 绘制缩小后的图
            g.dispose()
            image
        } else {
            logo
        }
        val graph = qrImage.createGraphics()
        val x: Int = (this.qrSize - width) / 2
        val y: Int = (this.qrSize - height) / 2
        graph.drawImage(logoImage, x, y, width, height, null)
        val shape: Shape = RoundRectangle2D.Float(
            x.toFloat(), y.toFloat(), width.toFloat(),
            width.toFloat(), 6.0f, 6.0f
        )
        graph.stroke = BasicStroke(3f)
        graph.draw(shape)
        graph.dispose()
        return this
    }

    fun logo(
        logo: InputStream,
        w: Int = 60,
        h: Int = 60,
        compress: Boolean = false
    ) = this.logo(ImageIO.read(logo), w, h, compress)

    /**
     * 设置 Graphics2D 属性  （抗锯齿）
     * @param graphics2D
     */
    private fun setGraphics2D(graphics2D: Graphics2D) {
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT)
        val s: Stroke = BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER)
        graphics2D.stroke = s
    }

    /**
     * 把带logo的二维码下面加上文字
     * @param image
     * @param text 文字
     * @param height 文字区域高度
     * @return
     */
    fun comment(
        text: String,
        height: Int = 40,
        font: Font
    ): QRCodeBuilder {
        val outImage = BufferedImage(this.qrSize, this.qrSize + height, BufferedImage.TYPE_INT_ARGB)
        val outGraphics = outImage.createGraphics()
        this.setGraphics2D(outGraphics)
        // 画二维码到新的面板
        outGraphics.drawImage(this.qrImage, 0, 0, this.qrImage.width, this.qrImage.height, null)
        // 画文字到新的面板
        val color = Color(0, 0, 0)
        outGraphics.color = color
        // 字体、字型、字号
        outGraphics.font = font
        //文字长度
        val strWidth = outGraphics.fontMetrics.stringWidth(text)
        //总长度减去文字长度的一半  （居中显示）
        val wordStartX = (this.qrSize - strWidth) / 2
        //height + (outImage.getHeight() - height) / 2 + 12
        val wordStartY = this.qrSize + 30
        // 画文字
        outGraphics.drawString(text, wordStartX, wordStartY)
        outGraphics.dispose()
        outImage.flush()
        this.qrImage = outImage
        return this
    }

    fun build(
        output: OutputStream,
        format: String = "PNG"
    ) {
        ImageIO.write(this.qrImage, format, output)
        this.qrImage.flush()
    }

    fun build(
        file: File,
        format: String = "PNG"
    ) {
        ImageIO.write(this.qrImage, format, file)
        this.qrImage.flush()
    }


}