package com.rollong.common.qrcode

import org.junit.Test
import java.awt.Font
import java.io.File
import java.net.URL
import javax.imageio.ImageIO

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 11:45
 * @project rollong-common
 * @filename com.rollong.common.qrcode.QrCodeTest.kt
 * @ide IntelliJ IDEA
 * @package
 * @description
 */
class QrCodeTest {

    private val content = "测试二维码字符串内容lkjalshkdkha;;😈🌙🌧;;@#$$"

    private val logo = "https://static.rollong.cn/upload/auto/2020.rollong.com/1616764877728/img/yangyi-qr.6db4b1c4.jpg"

    private fun tmpFile() = File.createTempFile("tmp", "qrcode.png")

    @Test
    fun testQRCode() {
        val file = tmpFile()
        QRCodeBuilder(
            content = this.content
        ).build(file)
        println(file.absolutePath)
    }

    @Test
    fun testQRCodeWithLogo() {
        val file = tmpFile()
        QRCodeBuilder(
            content = this.content
        ).logo(
            logo = ImageIO.read(URL(this.logo))
        ).build(file)
        println(file.absolutePath)
    }

    @Test
    fun testQRCodeWithLogoAndText() {
        val file = tmpFile()
        QRCodeBuilder(
            content = this.content
        ).logo(
            logo = ImageIO.read(URL(this.logo))
        ).comment(
            text = "底部文字测试",
            font = Font
                .createFont(Font.TRUETYPE_FONT, this.javaClass.classLoader.getResourceAsStream("fonts/simfang.ttf"))
                .deriveFont(25.0f)
        ).build(file)
        println(file.absolutePath)
    }
}