package com.rollong.common.storage.factory

import com.rollong.common.storage.StorageDisk
import com.rollong.common.storage.exception.DiskDriverNotFound

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 0:15
 * @project rollong-common
 * @filename StorageDiskFactoryRegistry.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.factory
 * @description
 */
class StorageDiskFactoryRegistry(
    val factories: MutableMap<String, StorageDiskFactory> = mutableMapOf()
) {

    fun add(factory: StorageDiskFactory): StorageDiskFactoryRegistry {
        this.factories[factory.name] = factory
        return this
    }

    @Throws(DiskDriverNotFound::class)
    fun build(name: String, driver: String, config: Map<String, String>): StorageDisk {
        val factory = this.factories[driver] ?: throw DiskDriverNotFound(name, driver)
        return factory.make(config)
    }
}