package com.rollong.common.storage

import com.rollong.common.storage.exception.FileTypeMismatch
import org.springframework.util.MimeType
import org.springframework.web.multipart.MultipartFile
import java.text.SimpleDateFormat
import java.util.*

object DiskUtil {

    private val df = SimpleDateFormat("yyyy/MM/dd")

    fun randomFilename(
        parentPath: String,
        ext: String,
        filenamePrefix: String? = null
    ): String {
        val now = Date()
        val filename = if (null == filenamePrefix) {
            UUID.randomUUID().toString().replace("-", "")
        } else {
            "$filenamePrefix-${System.currentTimeMillis()}"
        }
        return "${parentPath.trim('/', '.', ' ')}/${this.df.format(now)}/$filename.${ext.toLowerCase().trimStart('.')}"
    }

    @Throws(FileTypeMismatch::class)
    fun uploadImage(
        parentPath: String,
        file: MultipartFile,
        disk: StorageDisk
    ): String {
        val type = MimeType.valueOf(file.contentType!!).type
        if ("image" != type) {
            throw FileTypeMismatch("上传的文件不是图片类型")
        }
        val ext = try {
            file.originalFilename!!.substring(file.originalFilename!!.lastIndexOf("."))
        } catch (e: Exception) {
            ".jpg"
        }
        val path = this.randomFilename(parentPath, ext)
        disk.write(
            path = path,
            stream = file.inputStream
        )
        return path
    }
}