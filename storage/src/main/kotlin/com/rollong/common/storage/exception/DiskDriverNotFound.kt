package com.rollong.common.storage.exception

import com.rollong.common.exception.BaseException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 0:17
 * @project rollong-common
 * @filename DiskDriverNotFound.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.exception
 * @description
 */
class DiskDriverNotFound(disk: String, driver: String?) : BaseException(
    errCode = "common.storage.driver_not_found",
    code = 3,
    httpStatus = 500,
    message = "没有找到storageDisk[${disk}]的驱动:${driver}"
)