package com.rollong.common.storage.local

import com.rollong.common.storage.StorageDisk
import com.rollong.common.storage.factory.StorageDiskFactory

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 0:13
 * @project rollong-common
 * @filename LocalDiskFactory.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.local
 * @description
 */
class LocalDiskFactory : StorageDiskFactory {
    override val name: String = "local"
    override fun make(config: Map<String, String>): StorageDisk {
        return LocalDisk(
            config = LocalDiskConfig(
                publicURL = config["publicURL"] ?: throw RuntimeException("未指定publicURL"),
                localPath = config["localPath"] ?: throw RuntimeException("未指定localPath")
            )
        )
    }
}