package com.rollong.common.storage.factory

import com.rollong.common.storage.StorageDisk

interface StorageDiskFactory {
    val name: String
    fun make(config: Map<String, String>): StorageDisk
}