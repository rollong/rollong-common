package com.rollong.common.storage.local

data class LocalDiskConfig(
    var publicURL: String = "",
    var localPath: String = ""
)