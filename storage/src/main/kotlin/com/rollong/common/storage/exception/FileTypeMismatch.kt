package com.rollong.common.storage.exception

import com.rollong.common.exception.BaseException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-25 23:46
 * @project rollong-common
 * @filename FileTypeMismatch.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.exception
 * @description
 */
class FileTypeMismatch(message: String = "文件类型不匹配") : BaseException(
    errCode = "common.storage.file_type_mismatch",
    code = 2,
    httpStatus = 422,
    message = message
)