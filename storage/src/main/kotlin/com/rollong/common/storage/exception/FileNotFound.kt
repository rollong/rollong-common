package com.rollong.common.storage.exception

import com.rollong.common.exception.BaseException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-25 23:30
 * @project rollong-common
 * @filename FileNotFound.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.exception
 * @description
 */
class FileNotFound(file: String) : BaseException(
    errCode = "common.storage.file_not_found",
    code = 1,
    httpStatus = 404,
    message = "文件${file}没有找到"
)