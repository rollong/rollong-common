package com.rollong.common.storage

import com.rollong.common.storage.exception.DiskNotFound
import com.rollong.common.storage.factory.StorageDiskFactoryRegistry

class StorageDiskSet(
    var disks: MutableMap<String, Map<String, String>> = mutableMapOf(),
    val registry: StorageDiskFactoryRegistry
) {
    @Throws(DiskNotFound::class)
    fun getStorageDisk(name: String): StorageDisk {
        this.disks[name]?.let {
            return this.registry.build(name, it["driver"] ?: "default", it)
        }
        throw DiskNotFound(name)
    }
}



