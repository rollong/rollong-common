package com.rollong.common.storage.local

import com.rollong.common.storage.StorageDisk
import com.rollong.common.storage.exception.FileNotFound
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.concurrent.TimeUnit

class LocalDisk(
    private val config: LocalDiskConfig
) : StorageDisk {

    private val publicURL
        get() = config.publicURL.trimEnd('/') + '/'
    private val localPath
        get() = config.localPath.trimEnd('/') + '/'

    override fun read(path: String): InputStream {
        val filename = this.localPath + this.trimPath(path)
        try {
            return File(filename).inputStream()
        } catch (e: FileNotFoundException) {
            throw FileNotFound("找不到文件$filename")
        }
    }

    override fun write(path: String, stream: InputStream) {
        val filename = this.localPath + this.trimPath(path)
        val file = File(filename)
        if (!file.parentFile.exists()) {
            file.parentFile.mkdirs()
        }
        if (!file.exists()) {
            file.createNewFile()
        }
        try {
            stream.copyTo(file.outputStream())
        } catch (e: Throwable) {
            throw e
        } finally {
            stream.close()
        }
    }

    override fun publicURL(path: String, style: String?, timeout: Long, timeUnit: TimeUnit): String {
        return this.publicURL + this.trimPath(path)
    }

    override fun delete(path: String) {
        val filename = this.localPath + this.trimPath(path)
        val file = File(filename)
        if (file.exists() && file.isFile) {
            file.delete()
        }
    }

    override fun exists(filename: String): Boolean {
        val fullname = this.localPath + this.trimPath(filename)
        val file = File(fullname)
        return file.exists() && file.isFile
    }

    private fun trimPath(path: String) = path.trimStart('/', '.')

    override fun list(path: String, limit: Int?): List<StorageDisk.FileData> {
        val filePath = this.localPath + this.trimPath(path)
        val file = File(filePath)
        if (file.isDirectory) {
            val files = file.listFiles()?.filter { it.isFile }?.sortedByDescending { it.lastModified() } ?: listOf()
            return if (null != limit) files.take(limit).map {
                StorageDisk.FileData(
                    name = it.name,
                    createdAt = it.lastModified()
                )
            } else files.map {
                StorageDisk.FileData(
                    name = it.name,
                    createdAt = it.lastModified()
                )
            }
        } else {
            return listOf()
        }
    }
}