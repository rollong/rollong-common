package com.rollong.common.storage.exception

import com.rollong.common.exception.BaseException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-26 0:17
 * @project rollong-common
 * @filename DiskNotFound.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.storage.exception
 * @description
 */
class DiskNotFound(disk: String) : BaseException(
    errCode = "common.storage.disk_not_found",
    code = 3,
    httpStatus = 500,
    message = "没有找到storageDisk配置:${disk}"
)