package com.rollong.common.exception.auth

import com.rollong.common.exception.BaseException
import com.rollong.common.exception.ExceptionConstant

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 11:53
 * @project rollong-auth
 * @filename UnauthenticatedException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception.auth
 * @description
 */
open class UnauthenticatedException(message: String = "您没有相关权限") : BaseException(
    errCode = ExceptionConstant.AUTH_UNAUTHENTICATED,
    code = 403,
    httpStatus = 403,
    message = message
)