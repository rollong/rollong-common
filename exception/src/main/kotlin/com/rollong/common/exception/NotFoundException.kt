package com.rollong.common.exception

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 13:33
 * @project rollong-common
 * @filename NotFoundException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception
 * @description
 */
open class NotFoundException(message: String = "没有找到") : BaseException(
    code = 404,
    errCode = ExceptionConstant.NOT_FOUND,
    message = message,
    httpStatus = 200
)