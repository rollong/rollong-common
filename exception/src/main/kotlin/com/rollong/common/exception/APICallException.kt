package com.rollong.common.exception

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 15:04
 * @project rollong-common
 * @filename APICallException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception
 * @description
 */
open class APICallException(
    errCode: String = ExceptionConstant.API_CALL,
    code: Int = 500,
    message: String = "",
    httpStatus: Int = 500,
    cause: Throwable? = null,
    var api: String? = null,
    var serverErrCode: String? = null,
    var serverErrMessage: String? = null,
    var serverErrDescription: String? = null,
    var request: Any? = null,
    var response: Any? = null
) : BaseException(
    errCode = errCode,
    code = code,
    message = message,
    httpStatus = httpStatus,
    cause = cause
)