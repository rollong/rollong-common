package com.rollong.common.exception

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 13:33
 * @project rollong-common
 * @filename ExceptionConstant.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception
 * @description
 */
object ExceptionConstant {
    const val OK = "ok"
    const val FAIL = "fail"
    const val NOT_FOUND = "fail.not_found"
    const val BAD_REQUEST = "fail.bad_request"
    const val API_CALL = "fail.api_call"
    const val AUTH_UNAUTHORIZED = "fail.auth.unauthorized"
    const val AUTH_UNAUTHENTICATED = "fail.auth.unauthenticated"
}