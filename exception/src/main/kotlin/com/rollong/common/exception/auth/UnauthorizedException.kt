package com.rollong.common.exception.auth

import com.rollong.common.exception.BaseException
import com.rollong.common.exception.ExceptionConstant

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 11:51
 * @project rollong-auth
 * @filename UnauthorizedException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception.auth
 * @description
 */
open class UnauthorizedException(message: String = "认证失败") : BaseException(
    errCode = ExceptionConstant.AUTH_UNAUTHORIZED,
    code = 401,
    httpStatus = 401,
    message = message
)