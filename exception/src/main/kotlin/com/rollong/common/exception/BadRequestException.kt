package com.rollong.common.exception

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 13:35
 * @project rollong-common
 * @filename BadRequestException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception
 * @description
 */
open class BadRequestException(message: String = "请求格式有误") : BaseException(
    code = 400,
    errCode = ExceptionConstant.BAD_REQUEST,
    message = message,
    httpStatus = 400
)