package com.rollong.common.exception

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-12-25 23:31
 * @project rollong-common
 * @filename BaseException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.exception
 * @description 报错的基类
 */
open class BaseException(
    open var errCode: String = ExceptionConstant.FAIL,
    open var code: Int = 500,
    open var httpStatus: Int = 500,
    override var message: String? = null,
    override var cause: Throwable? = null
) : RuntimeException()